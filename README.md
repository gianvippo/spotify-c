## Spotify C

Il progetto svolta simula e ricorda la piattaforma di musica online Spotify nel linguaggio C.

## Funzioni

- [ ] Creazione e rimozione di utenti con ruolo di Admin o Utente semplice, con relativa generazione di password per login
- [ ] Creazione, modifica e rimozione di Artisti
- [ ] Funzione di Ascolto
- [ ] Reccomender System
