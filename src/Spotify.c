//Programma di Simulazione Spotify creato da Gianvito Palmisano

//Dichiarazione delle librerie per l'utilizzo completo del programma

#include "prototipi.h"

int main(void) {

	int a, b, c, n;	//dichiarazione controlli menu
	int ca, cb, cc;	//dichiarazione controlli sottomenu
	char *admin = "admin";
	char ins_password[6] = {0};
	FILE *stptr = 0;  //dichiarazione e inizializzazione puntatore

	setvbuf(stdout, NULL, _IONBF, 0);

	a = 0;

	while (a != 2) {	//ciclo menu principale

		system("color A");  //colore verde

		system("cls");	//clear screen

		printf("\n	SPOTIFY - La musica e' per tutti.\n\n"
				"   - 1. MENU\n"
				"   - 2. USCITA\n\n");

		ca = 0;

		while (ca != 1) {  //ciclo controllo input

			scanf("%d", &a);	//acquisizione valore per accesso menu

			if (a < 0 || a > 2) {	//se valore esterno a 1 o 3, stampa errore

				ca = 0;
				printf("ERRORE, REINSERIRE SCELTA.\n");

			} else
				ca = 1;	//altrimenti se input corretto esci dal ciclo
		}

		if (a == 0) {	//se a==1 entra nel amministratore


			printf("Inserire password amministratore: ");

			n = 0;
			while(n != 1) {

				fflush(stdin);
				gets(ins_password);	//acquisizione nome artista

				if(!strcmp(ins_password, admin)) n++;
				else printf("ERRORE, PASSWORD ERRATA.\n");

			}

			b = 0;

			while (b != 3) {	//ciclo menu amministatore

				system("cls");

				printf("\n	MENU AMMINISTRATORE\n\n"
						"   - 1. MENU ARTISTI\n   - 2. MENU UTENTI\n   - 3. USCITA\n\n");

				cb = 0;

				while (cb != 1) {	//ciclo controllo input

					scanf("%d", &b);	//acquisizione valore per accesso menu

					if (b < 1 || b > 3) {	//se valore esterno a 1 o 3, stampa errore

						cb = 0;
						printf("ERRORE, REINSERIRE SCELTA.\n");

					} else
						cb = 1;	//altrimenti se input corretto esci dal ciclo
				}

				if (b == 1) {	//se b==1 entra nel menu artisti

					c = 0;

					while (c != 6) {	//ciclo menu artisti

						system("cls");

						printf("\n	MENU ARTISTI.\n\n   - 1. Crea File Artisti\n"
								"   - 2. Aggiungi Artisti\n   - 3. Visualizza Artisti\n"
								"   - 4. Modifica Artista\n   - 5. Eliminazione\n   - 6. Uscita\n\n");

						cc = 0;

						while (cc != 1) {	//ciclo controllo input

							scanf("%d", &c);	//acquisizione valore per accesso menu

							if (c < 1 || c > 6) {	//se valore esterno a 1 o 6, stampa errore

								cc = 0;
								printf("ERRORE, REINSERIRE SCELTA.\n");

							} else
								cc = 1;	//altrimenti se input corretto esci dal ciclo
						}

						if (c == 1)
							creazione_artisti(stptr); //chiamata funzione creazione file artisti
						if (c == 2)
							aggiunta_artisti(stptr); //chiamata funzione aggiunta artisti
						if (c == 3)
							visualizzazione_artisti(stptr); //chiamata funzione visualizzazione artisti
						if (c == 4)
							modifica_artisti(stptr); //chiamata funzione modifica artisti
						if (c == 5)
							eliminazione_artisti(stptr); //chiamata funzione eliminazione artisti

					}
				}

				if (b == 2) {	//se b==2 entra nel menu utenti

					c = 0;

					while (c != 9) {	//ciclo menu utenti

						system("cls");

						printf("\n	MENU UTENTI\n\n   - 1. Crea File Utenti.\n   - 2. Aggiungi Utenti\n   - 3. Visualizza Utenti\n"
								"   - 4. Modifica Utente\n   - 5. Eliminazione Utente\n   - 6. Visualizza Profilo Utente\n"
								"   - 7. Modifica Preferenza Utente\n   - 8. Elimina Preferenza Utente\n   - 9. Uscita\n");

						cc = 0;

						while (cc != 1) {	//ciclo controllo input

							scanf("%d", &c);	//acquisizione valore per accesso menu

							if (c < 1 || c > 9) {	//se valore esterno a 1 o 6, stampa errore

								cc = 0;
								printf("ERRORE, REINSERIRE SCELTA.\n");

							} else
								cc = 1;	//altrimenti se input corretto esci dal ciclo
						}

						if (c == 1)
							creazione_utenti(stptr); //chiamata funzione creazione file utenti
						if (c == 2)
							aggiunta_utenti(stptr); //chiamata funzione aggiunta utenti
						if (c == 3)
							visualizzazione_utenti(stptr); //chiamata funzione visualizzazione utenti
						if (c == 4)
							modifica_utenti(stptr); //chiamata funzione modifica utenti
						if (c == 5)
							eliminazione_utenti(stptr); //chiamata funzione eliminazione utenti

						if (c == 6) {
							login_admin(stptr);			//chiamata funzione login per admin
							profilo(stptr);
							system("pause");
						}
						if (c == 7) {
							login_admin(stptr);			//chiamata funzione login per admin
							mod_pref_utente(stptr);
							system("pause");
						}
						if (c == 8) {
							login_admin(stptr);			//chiamata funzione login per admin
							del_pref_utente(stptr);
							system("pause");
						}

					}
				}
			}
		}

		if (a == 1) {	//se a==1 entra nel menu utente

			b = 0;

			while (b != 3) {	//ciclo menu utente

				system("cls");

				printf("\n	SPOTIFY\n");
				printf("\n     MENU UTENTE\n\n   - 1. LOGIN\n   - 2. REGISTRAZIONE\n   - 3. USCITA\n\n");

				cb = 0;

				while (cb != 1) {	//ciclo controllo input

					scanf("%d", &b);	//acquisizione valore per accesso menu

					if (b < 1 || b > 3) {	//se valore esterno a 1 o 3, stampa errore

						cb = 0;
						printf("ERRORE, REINSERIRE SCELTA.\n");

					} else
						cb = 1;	//altrimenti se input corretto esci dal ciclo
				}

				if (b == 1) {	//se b==1 entra nel menu login

					login(stptr);	//chiamata funzione login

					c = 0;

					while (c != 9) {	//ciclo menu login

						calcolo_utenti(stptr);	//chiamata funzione calcolo numero utenti
						calcolo_artisti(stptr);	//chiamata funzione calcolo numero artisti

						system("cls");

						printf("\n	SPOTIFY\n");
						printf("\n Cosa vorresti fare oggi?\n\n");

						reccomender_system(stptr);

						printf("   - 1. Ricerca.\n   - 2. Ascolto.\n   - 3. Visualizza Profilo.\n"
								"   - 4. Modifica Preferenza\n   - 5. Elimina Preferenza\n"
								"   - 6. Classifica per Ascolti\n   - 7. Classifica per Like\n   - 8. Classifica per Unlike\n"
								"   - 9. Log Out\n\n");

						cc = 0;

						while (cc != 1) {	//ciclo controllo input

							scanf("%d", &c);	//acquisizione valore per accesso menu

							if (c < 1 || c > 9) {	//se valore esterno a 1 o 3, stampa errore

								cc = 0;
								printf("ERRORE, REINSERIRE SCELTA.\n");

							} else
								cc = 1;	//altrimenti se input corretto esci dal ciclo
						}

						if (c == 1)	//se c==1 chiamata funzione ricerca

							ricerca(stptr);

						if (c == 2)	//se c==2 chiamata funzione ascolto

							ascolto(stptr);

						if (c == 3)	//se c==5 chiamata funzione profilo

							profilo(stptr);

						if (c == 4)	//se c==3 chiamata funzione modifica preferenza

							mod_pref_utente(stptr);

						if (c == 5)	//se c==4 chiamata funzione cancellazione preferenza

							del_pref_utente(stptr);

						if(c==6) ordinamento_listen(stptr);	//se c==6 chiamata funzione per ordinamento listen

						if(c==7) ordinamento_like(stptr);	//se c==7 chiamata funzione per ordinamento like

						if(c==8) ordinamento_unlike(stptr);	//se c==8 chiamata funzione per ordinamento unlike
					}
				}

				if (b == 2) {	//se b==2 chiamata funzione registrazione utente

					registrazione(stptr);

				}

			}
		}
	}

	return 0;	//la funzione main restituisce 0

}

//fine del main



