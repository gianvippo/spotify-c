/*
 * prototipi.c
 *
 *  Created on: 10 set 2017
 *      Author: gianvito
 */


#include "prototipi.h"

//________________________________________________________________________________________________________

//dichiarazione e inizializzazione varibili di tipo struct
artist artista = { 0 };
artist artistatemp = { 0 }; 		//struct per confronto artista
user utente = { 0 };
user utentetemp = { 0 }; 			//struct per confronto utente
preference preferenza = { 0 };
preference preferenzatemp = { 0 };	//struct per confronto preferenza

int i, j, k, s, z, n, x, y;	//variabili contatore
int uguale = 0;				//variabile utile per verificare l'uguaglianza
int id_fin = 0;				//variabile per id finale
int id_utente = 0;			//variabile per id utente
int controllo = 0;			//variabile controllo scelta utente
int gen = 0;				//veriabile per generatore password
int contnum = 0;			//variabile contatore numeri
int contspec = 0;			//variabile contatore caratteri speciali
int pass = 0;				//variabile contatore password
int contpass = 0;			//variabile contatore caratteri password
int ins = 0;				//variabile per inserimento livello sicurezza password
int reins = 0;				//variabile per reinserimento
char *listen = "listen";	//variabile per preferenza listen
char *like = "like";   		//variabile per preferenza like
char *unlike = "unlike"; 	//variabile per preferenza unlike
int reccomender = 0;		//variabile per similarit� artista

//_______________________________________________________________________________________________________

int risultati(void) {	//funzione stampa risultati

	//la funzione permette la stampa dei risultati, ogni campo viene analizzato e le posizioni interessate dello
	//array convertite. Se presente un underscore viene convertito in " ", il carattere immediamente successivo
	//viene convertito in maiuscolo

	k = 0;

	if (isalpha(artista.nome[k]))
	{
		artista.nome[k] = toupper(artista.nome[k]);
	}

	while (artista.nome[k]) {
		if (artista.nome[k] == '_')
		{

			artista.nome[k] = ' ';
			k++;
			if (isalpha(artista.nome[k]))
				artista.nome[k] = toupper(artista.nome[k]);

			k--;
		}
		k++;
	}

	k = 0;
	if (isalpha(artista.genere[k]))
		artista.genere[k] = toupper(artista.genere[k]);
	while (artista.genere[k]) {
		if (artista.genere[k] == '_') {
			artista.genere[k] = ' ';

			k++;

			if (isalpha(artista.genere[k]))
				artista.genere[k] = toupper(artista.genere[k]);

			k--;
		}
		k++;
	}

	k = 0;
	if (isalpha(artista.produttore[k]))
		artista.produttore[k] = toupper(artista.produttore[k]);
	while (artista.produttore[k]) {
		if (artista.produttore[k] == '_') {
			artista.produttore[k] = ' ';

			k++;

			if (isalpha(artista.produttore[k]))
				artista.produttore[k] = toupper(artista.produttore[k]);

			k--;
		}
		k++;
	}

	k = 0;
	if (isalpha(artista.nazionalita[k]))
		artista.nazionalita[k] = toupper(artista.nazionalita[k]);
	while (artista.nazionalita[k]) {
		if (artista.nazionalita[k] == '_') {
			artista.nazionalita[k] = ' ';

			k++;

			if (isalpha(artista.nazionalita[k]))
				artista.nazionalita[k] = toupper(artista.nazionalita[k]);

			k--;
		}
		k++;
	}

	printf("%-10d %-25s %-15s %-30s %-15s %-10d\n", artista.id_artista,
			artista.nome, artista.genere, artista.produttore,
			artista.nazionalita, artista.anno_attivita);	//stampa dei risultati

	return 0;
}

char conversione(char c) { //funzione conversione caratteri


	if (isalpha(c))
	{  //se carattere passato alla funzione maiuscolo converte in minuscolo
		c = tolower(c);
	}

	if (c == ' ') 	 //se carattere passato alla funzione == " " converte in underscore
		c = '_';

	return c;	//restituisce c
}

int creazione_artisti(FILE *ptr) { //funzione per creazione file artisti e aggiunta di quest'ultimi

	system("cls");
	printf("\n	SPOTIFY - Creazione file\n\n");
	printf("Vuoi creare il database degli artisti? Sovrascrivera' il file esistente.\nDigita 1 per proseguire, 0 per tornare al menu'\n");

	controllo = 1;

	while (controllo != 0) {	//cicla fin quando controllo != 0

		scanf("%d", &controllo);	//acquisizione input per controllo

		if (controllo != 0 && controllo != 1)	//se controllo != 0 o 1 stampa errore

			printf("ERRORE, REINSERIRE SCELTA.\n");

		if (controllo == 1) {	//se controllo == 1 crea file artisti

			ptr = fopen("artist.dat", "wb");	//apertura file in scrittura binaria distruttiva
			fclose(ptr);						//chiusura file

			if ((ptr = fopen("artist.dat", "rb+")) == NULL) {	//apertura file in scrittura binaria

				printf("ERRORE FILE.\n");

			} else {

				i = 0;	//azzeramento variabili di controllo
				j = 0;

				while (i != 1) {	//cicla fino a controllo != 1

					artista.id_artista = j;		//memorizza in j indirizzo artista
					artista.id_artista++; 		//aumenta di 1 valore id artista

					system("cls");
					printf("\n	SPOTIFY - Aggiunta Artista\n\n");
					printf("Inserire Nome Artista.\n");

					n = 0;

					while (n != 1) {	//cicla fino a controllo != 1

						uguale = 0;

						k = 0;
						while (artista.nome[k]) {	//ciclo azzeramento array
							artista.nome[k] = 0;
							k++;
						}

						fflush(stdin);
						gets(artista.nome);	//acquisizione nome artista

						if (artista.nome[25] != 0)	//controllo lunghezza array non superiore a 25
							//se posizione 25 occupata stampa errore
							printf("ERRORE. IL CAMPO NOME NON PUO' SUPERARE I 25 CARATTERI.\n");

						k = 0;
						while (artista.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
							artista.nome[k] = conversione(artista.nome[k]);
							k++;
						}

						k = 0;

						rewind(ptr);										//posiziona puntatore all'inizio file
						while (!feof(ptr)) {								//cicla puntatore fino a fine file
							fseek(ptr, k * sizeof(artista), SEEK_SET);		//posiziona puntatore a struct in posizione k
							fread(&artistatemp, sizeof(artista), 1, ptr);	//carica in memoria la struct artista temporaneo
							if (!strcmp(artistatemp.nome, artista.nome)) {	//confronto artistatemp.nome con artista.nome
								printf("\n!!!ARTISTA GIA' ESISTENTE!!!\n");	//se uguale stampa messaggio e incrementa variabile uguale
								uguale = 1;
								break;
							}
							k++;
						}
						if (uguale == 0 && artista.nome[25] == 0)	//se controllo uguale e posizione 25 array vuota, esci dal ciclo
							n++;
					}

					printf("\nInserire Genere.\n");

					n = 0;

					while (n != 1) {	//cicla fino a controllo != 1

						k = 0;
						while (artista.genere[k]) {	//ciclo azzeramento array
							artista.genere[k] = 0;
							k++;
						}

						fflush(stdin);
						gets(artista.genere);	//acquisizione genere artista

						if (artista.genere[15] != 0)	//controllo lunghezza array non superiore a 15
							//se posizione 15 occupata stampa errore
							printf("ERRORE. IL CAMPO GENERE NON PUO' SUPERARE I 15 CARATTERI.\n");

						k = 0;
						while (artista.genere[k]) {	//conversione array per scrittura in file, converte ogni posizione
							artista.genere[k] = conversione(artista.genere[k]);
							k++;
						}

						if (artista.genere[15] == 0) //se posizione 15 array vuota esce dal ciclo
							n++;
					}

					printf("\nInserire Produttore.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						k = 0;
						while (artista.produttore[k]) {	//ciclo azzeramento array
							artista.produttore[k] = 0;
							k++;
						}

						fflush(stdin);
						gets(artista.produttore);	//acquisizione produttore artista

						if (artista.produttore[30] != 0)	//controllo lunghezza array non superiore a 30
							//se posizione 30 occupata stampa errore
							printf("ERRORE. IL CAMPO PRODUTTORE NON PUO' SUPERARE I 30 CARATTERI.\n");

						k = 0;
						while (artista.produttore[k]) {	//conversione array per scrittura in file, converte ogni posizione
							artista.produttore[k] = conversione(
									artista.produttore[k]);
							k++;
						}

						if (artista.produttore[30] == 0) //se posizione 30 array vuota esce dal ciclo
							n++;
					}

					printf("\nInserire Nazionalita'.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						k = 0;
						while (artista.nazionalita[k]) {	//ciclo azzeramento array
							artista.nazionalita[k] = 0;
							k++;
						}

						fflush(stdin);
						gets(artista.nazionalita);	//acquisizione nazionalit� artista

						if (artista.nazionalita[15] != 0)	//controllo lunghezza array non superiore a 15
							//se posizione 15 occupata stampa errore
							printf("ERRORE. IL CAMPO NAZIONALITA' NON PUO' SUPERARE I 15 CARATTERI.\n");

						k = 0;
						while (artista.nazionalita[k]) {	//conversione array per scrittura in file, converte ogni posizione
							artista.nazionalita[k] = conversione(
									artista.nazionalita[k]);
							k++;
						}

						if (artista.nazionalita[15] == 0) //se posizione 15 array vuota esce dal ciclo
							n++;
					}

					printf("\nInserire Anno Attivita'.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						scanf("%d", &artista.anno_attivita);	//acquisizione anno
						if (artista.anno_attivita < 1900 || artista.anno_attivita > 2017) {
							//se anno < 1900 o > 2017 stampa errore
							printf("Errore: Anno Minimo 1900.\n");

						} else {
							n++; //altrimenti esci dal ciclo
						}
					}

					printf("\nVuoi inserire questo artista? Digita 1 per confermare, 0 per uscire.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						scanf("%d", &s);	//acquisizone input

						if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
							n++;
						else
							printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
					}

					if (s == 0) {	//se s == 0 non aggiungere artista ed esci dal ciclo principale
						printf("\n!!!ARTISTA NON AGGIUNTO!!!\n");
						system("pause");
						i++;
					}

					if (s == 1) { //se s == 1 posiziona puntatore in posizione j e scrive struct nel file

						fseek(ptr, j * sizeof(artista), SEEK_SET);
						fwrite(&artista, sizeof(artista), 1, ptr);
						printf("\n!!!ARTISTA AGGIUNTO!!!\n");

						printf("\nDigita 1 per inserire i dati di un altro artista, 0 per tornare al menu.\n");

						n = 0;
						while (n != 1) {	//cicla fino a controllo != 1

							scanf("%d", &s);	//acquisizione input

							if (s == 1 || s == 0)
								n++; //se s == 0 o 1 esci dal ciclo

							else
								printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
						}

						if (s == 0) { //se s == 0 esci dal ciclo principale
							i++;
						}
						j++; //incremento indirizzo artista
					}
				}
				fclose(ptr); //chiusura file
			}
			controllo = 0; //reset variabile controllo
		}
	}

	return 0;

}

int aggiunta_artisti(FILE *ptr) { //funzione aggiunta artisti

	//azzeramento variabili di controllo
	i = 0;
	s = 0;
	j = 0;

	if ((ptr = fopen("artist.dat", "rb+")) == NULL) {	//apertura file in scrittura binaria

		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&artista, sizeof(artista), 1, ptr); 	//lettura struct
		}

		j = artista.id_artista;	//salva in j l'ultimo id artista

		i = 0;
		while (i != 1) {	//cicla fino a controllo != 1

			system("cls");
			printf("\n	SPOTIFY - Aggiunta Artista\n\n");

			artista.id_artista = j;	//assegna a id artista nella struct ultimo id letto e
			artista.id_artista++;	//ne aumenta il valore di 1

			printf("Inserire Nome Artista.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				uguale = 0;

				k = 0;
				while (artista.nome[k]) {	//ciclo azzeramento array
					artista.nome[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(artista.nome);	//acquisizione nome artista

				if (artista.nome[25] != 0)	//controllo lunghezza array non superiore a 25
					//se posizione 25 occupata stampa errore
					printf("ERRORE. IL CAMPO NOME NON PUO' SUPERARE I 25 CARATTERI.\n");

				k = 0;
				while (artista.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
					artista.nome[k] = conversione(artista.nome[k]);
					k++;
				}

				k = 0;

				rewind(ptr);										//posiziona puntatore all'inizio file
				while (!feof(ptr)) {								//cicla puntatore fino a fine file
					fseek(ptr, k * sizeof(artista), SEEK_SET);		//posiziona puntatore a struct in posizione k
					fread(&artistatemp, sizeof(artista), 1, ptr);	//carica in memoria la struct artista temporaneo
					if (!strcmp(artistatemp.nome, artista.nome)) {	//confronto artistatemp.nome con artista.nome
						printf("\n!!!ARTISTA GIA' ESISTENTE!!!\n");	//se uguale stampa messaggio e incrementa variabile uguale
						uguale = 1;
						break;
					}
					k++;
				}
				if (uguale == 0 && artista.nome[25] == 0)	//se controllo uguale e posizione 25 array vuota, esci dal ciclo
					n++;
			}

			printf("\nInserire Genere.\n");

			n = 0;

			while (n != 1) {	//cicla fino a controllo != 1

				k = 0;
				while (artista.genere[k]) {	//ciclo azzeramento array
					artista.genere[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(artista.genere);	//acquisizione genere artista

				if (artista.genere[15] != 0)	//controllo lunghezza array non superiore a 15
					//se posizione 15 occupata stampa errore
					printf("ERRORE. IL CAMPO GENERE NON PUO' SUPERARE I 15 CARATTERI.\n");

				k = 0;
				while (artista.genere[k]) {	//conversione array per scrittura in file, converte ogni posizione
					artista.genere[k] = conversione(artista.genere[k]);
					k++;
				}

				if (artista.genere[15] == 0) //se posizione 15 array vuota esce dal ciclo
					n++;
			}

			printf("\nInserire Produttore.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				k = 0;
				while (artista.produttore[k]) {	//ciclo azzeramento array
					artista.produttore[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(artista.produttore);	//acquisizione produttore artista

				if (artista.produttore[30] != 0)	//controllo lunghezza array non superiore a 30
					//se posizione 30 occupata stampa errore
					printf("ERRORE. IL CAMPO PRODUTTORE NON PUO' SUPERARE I 30 CARATTERI.\n");

				k = 0;
				while (artista.produttore[k]) {	//conversione array per scrittura in file, converte ogni posizione
					artista.produttore[k] = conversione(
							artista.produttore[k]);
					k++;
				}

				if (artista.produttore[30] == 0) //se posizione 30 array vuota esce dal ciclo
					n++;
			}

			printf("\nInserire Nazionalita'.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				k = 0;
				while (artista.nazionalita[k]) {	//ciclo azzeramento array
					artista.nazionalita[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(artista.nazionalita);	//acquisizione nazionalit� artista

				if (artista.nazionalita[15] != 0)	//controllo lunghezza array non superiore a 15
					//se posizione 15 occupata stampa errore
					printf("ERRORE. IL CAMPO NAZIONALITA' NON PUO' SUPERARE I 15 CARATTERI.\n");

				k = 0;
				while (artista.nazionalita[k]) {	//conversione array per scrittura in file, converte ogni posizione
					artista.nazionalita[k] = conversione(
							artista.nazionalita[k]);
					k++;
				}

				if (artista.nazionalita[15] == 0) //se posizione 15 array vuota esce dal ciclo
					n++;
			}

			printf("\nInserire Anno Attivita'.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				scanf("%d", &artista.anno_attivita);	//acquisizione anno
				if (artista.anno_attivita < 1900 || artista.anno_attivita > 2017) {
					//se anno < 1900 o > 2017 stampa errore
					printf("Errore: Anno Minimo 1900.\n");

				} else {
					n++; //altrimenti esci dal ciclo
				}
			}

			printf("\nVuoi inserire questo artista? Digita 1 per confermare, 0 per uscire.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				scanf("%d", &s);	//acquisizone input

				if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
					n++;
				else
					printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
			}

			if (s == 0) {	//se s == 0 non aggiungere artista ed esci dal ciclo principale
				printf("\n!!!ARTISTA NON AGGIUNTO!!!\n");
				system("pause");
				i++;
			}

			else {	//altrimenti posiziona puntatore in posizione j e scrive struct nel file
				fseek(ptr, j * sizeof(artista), SEEK_SET);
				fwrite(&artista, sizeof(artista), 1, ptr);
				printf("\n!!!ARTISTA AGGIUNTO!!!\n");

				printf("\nDigita 1 per inserire i dati di un altro artista, 0 per tornare al menu.\n");

				n = 0;
				while (n != 1) {	//cicla fino a controllo != 1

					scanf("%d", &s);	//acquisizione input

					if (s == 1 || s == 0)
						n++; //se s == 0 o 1 esci dal ciclo

					else
						printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
				}

				if (s == 0) { //se s == 0 esci dal ciclo principale
					i++;
				}
				j++; //incremento indirizzo artista
			}
		}
		fclose(ptr); //chiusura file
	}
	return 0;
}

int visualizzazione_artisti(FILE *ptr) { //funzione visualizzazione artisti presenti

	int prim_id = 0;   //dichiarazione e inizializzazione variabile primo id da visualizzare
	int ultm_id = 0;   //dichiarazione e inizializzazione variabile ultimo id da visualizzare
	int cont_id = 0;   //dichiarazione e inizializzazione contatore id

	setvbuf(stdout, NULL, _IONBF, 0);

	if ((ptr = fopen("artist.dat", "rb")) == NULL) { //apertura file in lettura binaria

		printf("ERRORE FILE.\n");

	} else {

		artista.id_artista = 0;	//azzeramento id in memoria

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//lettura struct di ogni artista
			fread(&artista, sizeof(artista), 1, ptr);
		}

		id_fin = artista.id_artista;	//assegna a id_fin ultimo id letto

		system("cls");
		printf("\n	SPOTIFY - Visualizzazione Artisti\n\n");

		printf("Sono presenti %d artisti nel database.\n", id_fin);

		if (id_fin != 0) {	//se id_fin != 0 continua la visualizzazione

			printf("Per visualizzare un elenco di artisti inserire il Primo ID e l'Ultimo ID che si vuole visualizzare.\n\nInserire Primo ID.\n");

			while (cont_id != 1) {		//ciclo controllo id inserito

				scanf("%d", &prim_id);	//acquisizione primo id

				if (prim_id > id_fin)	//se id inserito > di id_fin stampa errore

					printf("ERRORE. L'ID INSERITO NON ESISTE.\n");

				else
					cont_id = 1; //altrimenti esci dal ciclo
			}

			printf("\nInserire Ultimo ID da visualizzare.\n");

			cont_id = 0; //azzeramento contatore

			while (cont_id != 1) {		//ciclo controllo id inserito

				scanf("%d", &ultm_id);	//acquisizione secondo id

				if (ultm_id > id_fin) 	//se id inserito > di id_fin stampa errore

					printf("ERRORE. L'ID INSERITO NON ESISTE.\n");

				if (ultm_id < prim_id) 	//se id inserito < di prim_id stampa errore

					printf("ERRORE. ULTIMO ID < PRIMO ID.\n");

				if (ultm_id <= id_fin && ultm_id >= prim_id)	//se condizioni soddisfatte esci dal ciclo
					cont_id = 1;
			}

			prim_id--;	//diminuisce di 1 le variabili per posizionarsi correttamente nel file
			ultm_id--;

			printf("\n%-10s %-25s %-15s %-30s %-15s %-10s\n", "ID ARTISTA",
					"NOME ARTISTA", "GENERE", "PRODUTTORE", "NAZIONALITA'",
					"ANNO DI ATTIVITA'");

			rewind(ptr);									//posiziona puntatore all'inizio file
			for (i = prim_id; i <= ultm_id; i++) { 			//ciclo for per caricare in memoria gli artisti indicati
				fseek(ptr, i * sizeof(artista), SEEK_SET);
				fread(&artista, sizeof(artista), 1, ptr);
				risultati();								//chiamata funzione risultati
			}

			printf("\n");

			system("pause");

			fclose(ptr); //chiusura file
		}
	}
	return 0;

}


int modifica_artisti(FILE *ptr) {	//funzione modifica artisti esistenti

	int trovato;	//variabile per controllo

	if ((ptr = fopen("artist.dat", "rb")) == NULL) { //apertura file in lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&artista, sizeof(artista), 1, ptr); 	//lettura struct
		}

		id_fin = artista.id_artista;	//assegna a id_fin ultimo id letto

		system("cls");
		printf("\n	SPOTIFY - Modifica Artista\n\n");

		n = 0;

		printf("Inserire Nome Artista da Modificare.\n");

		while (n != 1) {	//cicla fino a controllo != 1

			trovato = 0;

			fflush(stdin);
			gets(artista.nome);	//acquisizione nome artista

			k = 0;
			while (artista.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
				artista.nome[k] = conversione(artista.nome[k]);
				k++;
			}

			z = 0;
			rewind(ptr);										//posiziona puntatore all'inizio file
			while (!feof(ptr)) {								//cicla puntatore fino a fine file
				fseek(ptr, z * sizeof(artista), SEEK_SET);		//posiziona puntatore a struct in posizione z
				fread(&artistatemp, sizeof(artista), 1, ptr);	//carica in memoria la struct artista temporaneo
				if (!strcmp(artistatemp.nome, artista.nome)) {	//confronto artistatemp.nome con artista.nome
					printf("\n!!!ARTISTA TROVATO!!!\n"); 		//se uguale stampa messaggio e incrementa variabile trovato
					trovato = 1;
					break;
				}
				z++;	//incrementa contatore z per indirizzo artisti
			}

			if (trovato == 1)	//se trovato == 1 esci dal ciclo, altrimenti stampa errore
				n++;
			else
				printf("\n!!!ARTISTA NON TROVATO!!!\n");
		}
	}
	fclose(ptr);	//chiusura del file

	ptr = fopen("tmp.txt", "wb");	//apertura file in scrittura binaria distruttiva
	fclose(ptr);	//chiusura del file

	for (i = 0; i < z; i++) {	//ciclo for da indirizzo 0 a indirizzo trovato, con apertura del file artisti
		//in modalit� lettura, caricamento della struct in memoria, chiusura del file
		//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria
		//e chiusura del file

		if ((ptr = fopen("artist.dat", "rb")) == NULL) {
			printf("ERRORE FILE.\n");

		} else {
			fseek(ptr, i * sizeof(artista), SEEK_SET);
			fread(&artista, sizeof(artista), 1, ptr);
		}
		fclose(ptr);

		if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
			printf("ERRORE FILE.\n");
		} else {
			fseek(ptr, i * sizeof(artista), SEEK_SET);
			fwrite(&artista, sizeof(artista), 1, ptr);
		}
		fclose(ptr);
	}

	printf("\nInserire i Dati del Nuovo Artista\n");

	artista.id_artista = z;	//assegna a id_artista il valore di z e incrementa di 1 la variabile
	artista.id_artista++;

	if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in modalit� scrittura
		printf("ERRORE FILE.\n");

	} else {

		printf("\nInserire Nome Artista.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			uguale = 0;

			k = 0;
			while (artista.nome[k]) {	//ciclo azzeramento array
				artista.nome[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(artista.nome);	//acquisizione nome artista

			if (artista.nome[25] != 0)	//controllo lunghezza array non superiore a 25
				//se posizione 25 occupata stampa errore
				printf("ERRORE. IL CAMPO NOME NON PUO' SUPERARE I 25 CARATTERI.\n");

			k = 0;
			while (artista.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
				artista.nome[k] = conversione(artista.nome[k]);
				k++;
			}

			k = 0;

			rewind(ptr);										//posiziona puntatore all'inizio file
			while (!feof(ptr)) {								//cicla puntatore fino a fine file
				fseek(ptr, k * sizeof(artista), SEEK_SET);		//posiziona puntatore a struct in posizione k
				fread(&artistatemp, sizeof(artista), 1, ptr);	//carica in memoria la struct artista temporaneo
				if (!strcmp(artistatemp.nome, artista.nome)) {	//confronto artistatemp.nome con artista.nome
					printf("\n!!!ARTISTA GIA' ESISTENTE!!!\n");	//se uguale stampa messaggio e incrementa variabile uguale
					uguale = 1;									//incremento la variabile uguale
					break;
				}
				k++;
			}
			if (uguale == 0 && artista.nome[25] == 0)	//se controllo uguale e posizione 25 array vuota, esci dal ciclo
				n++;
		}

		printf("\nInserire Genere.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			k = 0;
			while (artista.genere[k]) {	//ciclo azzeramento array
				artista.genere[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(artista.genere);	//acquisizione genere artista

			if (artista.genere[15] != 0)	//controllo lunghezza array non superiore a 15
				//se posizione 15 occupata stampa errore
				printf("ERRORE. IL CAMPO GENERE NON PUO' SUPERARE I 15 CARATTERI.\n");

			k = 0;
			while (artista.genere[k]) {	//conversione array per scrittura in file, converte ogni posizione
				artista.genere[k] = conversione(artista.genere[k]);
				k++;
			}

			if (artista.genere[15] == 0) //se posizione 15 array vuota esce dal ciclo
				n++;
		}

		printf("\nInserire Produttore.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			k = 0;
			while (artista.produttore[k]) {	//ciclo azzeramento array
				artista.produttore[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(artista.produttore);	//acquisizione produttore artista

			if (artista.produttore[30] != 0)	//controllo lunghezza array non superiore a 30
				//se posizione 30 occupata stampa errore
				printf("ERRORE. IL CAMPO PRODUTTORE NON PUO' SUPERARE I 30 CARATTERI.\n");

			k = 0;
			while (artista.produttore[k]) {	//conversione array per scrittura in file, converte ogni posizione
				artista.produttore[k] = conversione(
						artista.produttore[k]);
				k++;
			}

			if (artista.produttore[30] == 0) //se posizione 30 array vuota esce dal ciclo
				n++;
		}

		printf("\nInserire Nazionalita'.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			k = 0;
			while (artista.nazionalita[k]) {	//ciclo azzeramento array
				artista.nazionalita[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(artista.nazionalita);	//acquisizione nazionalita artista

			if (artista.nazionalita[15] != 0)	//controllo lunghezza array non superiore a 15
				//se posizione 15 occupata stampa errore
				printf("ERRORE. IL CAMPO NAZIONALITA' NON PUO' SUPERARE I 15 CARATTERI.\n");

			k = 0;
			while (artista.nazionalita[k]) {	//conversione array per scrittura in file, converte ogni posizione
				artista.nazionalita[k] = conversione(
						artista.nazionalita[k]);
				k++;
			}

			if (artista.nazionalita[15] == 0) //se posizione 15 array vuota esce dal ciclo
				n++;
		}

		printf("\nInserire Anno Attivita'.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			scanf("%d", &artista.anno_attivita);	//acquisizione anno
			if (artista.anno_attivita < 1900 || artista.anno_attivita > 2017) {
				//se anno < 1900 o > 2017 stampa errore
				printf("Errore: Anno Minimo 1900.\n");

			} else {
				n++; //altrimenti esci dal ciclo
			}
		}
	}
	fclose(ptr);	//chiusura del file

	printf("\nVuoi modificare questo artista? Digita 1 per confermare, 0 per uscire.\n");

	controllo = 0;
	while (controllo != 1) {	//ciclo controllo inserimento

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			scanf("%d", &s);	//acquisizone input

			if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
				n++;
			else
				printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
		}

		if (s == 0) {	//se input == 0 stampa messaggio ed esce dal ciclo

			printf("\n!!!ARTISTA NON MODIFICATO!!!\n");
			system("pause");
			controllo = 1;
		}

		else {	//altrimenti procede

			if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {	//apertura file in modalit� scrittura binaria
				printf("ERRORE FILE.\n");

			} else {

				fseek(ptr, z * sizeof(artista), SEEK_SET);	//scrittura della struct in memoria in posizione z
				fwrite(&artista, sizeof(artista), 1, ptr);
			}

			fclose(ptr);	//chiusura del file

			z++;	//incrementa indirizzo

			for (i = z; i < id_fin; i++) {	//ciclo for da indirizzo z a id_fin, con apertura del file artisti
				//in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("artist.dat", "rb")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fread(&artista, sizeof(artista), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fwrite(&artista, sizeof(artista), 1, ptr);
				}
				fclose(ptr);
			}

			ptr = fopen("artist.dat", "wb");	//apertura del file in modalit� scrittura distruttiva
			fclose(ptr);						//chiusura del file

			for (i = 0; i < id_fin; i++) {	//ciclo for da indirizzo 0 a id_fin, con apertura del file tmp
				//in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file artisti in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("tmp.txt", "rb")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fread(&artista, sizeof(artista), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("artist.dat", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fwrite(&artista, sizeof(artista), 1, ptr);
				}
				fclose(ptr);
			}

			printf("\n!!!ARTISTA MODIFICATO CON SUCCESSO!!!\n");
			system("pause");
			controllo = 1;	//uscita dal ciclo
		}
	}
	return 0;

}

int eliminazione_artisti(FILE *ptr) {	//funzione eliminazione artisti esistenti

	int trovato;	 //variabile per controllo

	if ((ptr = fopen("artist.dat", "rb")) == NULL) { //apertura file in lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&artista, sizeof(artista), 1, ptr); 	//lettura struct
		}

		id_fin = artista.id_artista;	//assegna a id_fin ultimo id letto

		system("cls");
		printf("\n	SPOTIFY - Eliminazione Artista\n\n");
		n = 0;

		printf("Inserire Nome Artista da Eliminare.\n");

		while (n != 1) {	//cicla fino a controllo != 1

			trovato = 0;

			fflush(stdin);
			gets(artista.nome);	//acquisizione nome artista

			k = 0;
			while (artista.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
				artista.nome[k] = conversione(artista.nome[k]);
				k++;
			}

			z = 0;
			rewind(ptr);										//posiziona puntatore all'inizio file
			while (!feof(ptr)) {								//cicla puntatore fino a fine file
				fseek(ptr, z * sizeof(artista), SEEK_SET);		//posiziona puntatore a struct in posizione z
				fread(&artistatemp, sizeof(artista), 1, ptr);	//carica in memoria la struct artista temporaneo
				if (!strcmp(artistatemp.nome, artista.nome)) {	//confronto artistatemp.nome con artista.nome
					printf("\n!!!ARTISTA TROVATO!!!\n"); 				//se uguale stampa messaggio e incrementa variabile trovato
					trovato = 1;
					break;
				}
				z++;	//incrementa contatore z per indirizzo artisti
			}

			if (trovato == 1)	//se trovato == 1 esci dal ciclo, altrimenti stampa errore
				n++;
			else
				printf("\n!!!ARTISTA NON TROVATO. RIPROVARE!!!\n");
		}
	}
	fclose(ptr);	//chiusura del file

	printf("\nVuoi eliminare l'artista?\nDigita 1 per proseguire, 0 per tornare al menu.\n");

	controllo = 0;
	while (controllo != 1) {	//ciclo controllo inserimento

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			scanf("%d", &s);	//acquisizone input

			if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
				n++;
			else
				printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
		}

		if (s == 0) {	//se input == 0 stampa messaggio ed esce dal ciclo

			printf("\n!!!ARTISTA NON ELIMINATO!!!\n");
			system("pause");
			controllo = 1;
		}

		else {

			ptr = fopen("tmp.txt", "wb");	//apertura file in scrittura binaria distruttiva
			fclose(ptr);	//chiusura del file

			for (i = 0; i < z; i++) {	//ciclo for da indirizzo 0 a indirizzo trovato, con apertura del file artisti
				//in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("artist.dat", "rb")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {

					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fread(&artista, sizeof(artista), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {

					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fwrite(&artista, sizeof(artista), 1, ptr);
				}
				fclose(ptr);
			}

			n = z;	//assegna a n indirizzo z e incrementa di 1 n

			n++;
			for (i = n; i < id_fin; i++) {	//ciclo for da indirizzo n a id_fin, con apertura del file artisti
				//in modalit� lettura, caricamento della struct in memoria, decremento del valore id_artista chiusura del file
				//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria all'indirizzo z,
				//incremento di 1 del valore indirizzo z e chiusura del file

				if ((ptr = fopen("artist.dat", "rb")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {

					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fread(&artista, sizeof(artista), 1, ptr);
					artista.id_artista--;
				}
				fclose(ptr);

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {
					fseek(ptr, z * sizeof(artista), SEEK_SET);
					fwrite(&artista, sizeof(artista), 1, ptr);
				}
				z++;
				fclose(ptr);
			}

			ptr = fopen("artist.dat", "wb");	//apertura del file in modalit� scrittura distruttiva
			fclose(ptr);						//chiusura del file

			for (i = 0; i < id_fin; i++) {	//ciclo for da indirizzo 0 a id_fin, con apertura del file tmp
				//in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file artisti in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("tmp.txt", "rb")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fread(&artista, sizeof(artista), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("artist.dat", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(artista), SEEK_SET);
					fwrite(&artista, sizeof(artista), 1, ptr);
				}
				fclose(ptr);
			}

			printf("\n!!!ARTISTA ELIMINATO!!!\n");
			system("pause");
			controllo = 1;	//uscita dal ciclo
		}
	}
	return 0;

}

date inserimento_data() {	//funzione inserimento data

	int bisestile; 	 //dichiarazione variabile controllo anno bisestile
	int errore;  //dichiarazione variabile controllo errore
	int d = 0;   //dichiarazione variabile controllo ciclo
	date data; 	 //dichiarazione struct data

	while (d != 1) {

		bisestile = 0;
		errore = 0;

		scanf("%2d/%2d/%4d", &data.giorno, &data.mese, &data.anno);	//acquisizione struct data

		if (data.giorno > 31) {	//se giorno > 31 incrementa errore
			printf("ERRORE. I giorni non possono essere maggiori di 31.\n");
			errore++;
		}

		if (data.mese > 12) {	//se mese > 12 incrementa errore
			printf("ERRORE. I mesi non possono essere maggiori di 12.\n");
			errore++;
		}
		if (data.anno < 1900 || data.anno > 2017) { //se anno non � compreso tra valori stampa errore e incrementa errore
			printf("ERRORE. L'anno deve essere compreso tra il 1900 e il 2017.\n");
			errore++;
		}

		if ((data.mese == 4 || data.mese == 6 || data.mese == 9 //controllo mesi di 30 giorni
				|| data.mese == 11) && data.giorno > 30) {

			printf("ERRORE. Il mese inserito non supera i 30 giorni.\n");
			errore++;	//se condizioni soddisfatte incrementa errore
		}

		if (data.mese == 2) {	//controllo anno bisestile
			if ((data.anno % 4 == 0 && data.anno % 100 != 0)
					|| data.anno % 400 == 0)
				bisestile++;	//se condizioni soddisfatte incrementa bisestile

			if (((data.anno % 4 == 0 && data.anno % 100 != 0) //controllo mese febbraio con anno bisestile
					|| data.anno % 400 == 0) && data.giorno > 29) {
				printf("ERRORE. Il mese inserito � bisestile e non pu� superare i 29 giorni.\n");
				errore++;	//se condizioni soddisfatte incrementa errore
			}

			if (data.giorno > 28 && bisestile == 0) { //controllo mese febbraio con anno non bisestile
				printf("ERRORE. Il mese inserito non pu� superare i 28 giorni.\n");
				errore++;	//se condizioni soddisfatte incrementa errore
			}
		}

		if (errore == 0) //se contatore errore == 0 esci dal ciclo, altrimenti stampa erroe
			d++;
		else
			printf("Reinserire la data.\n");
	}

	return data;	//la funzione restituisce la struct data
}

int creazione_utenti(FILE *ptr) {	//funzione creazione file utenti e aggiunti di quest'ultimi

	system("cls");
	printf("\n	SPOTIFY - Creazione File Utenti\n\n");
	printf("Vuoi creare il database degli utenti? Sovrascrivera' il file esistente.\nDigita 1 per proseguire, 0 per tornare al menu'.\n");

	controllo = 1;
	while (controllo != 0) {	//ciclo controllo input

		scanf("%d", &controllo);	//acquisizione input

		if (controllo != 0 && controllo != 1)	//se controllo != 1 o 0 stampa errore
			printf("ERRORE. REINSERIRE SCELTA.\n");

		if (controllo == 1) {	//se controllo == 1 continua funzione

			ptr = fopen("user.dat", "wb");	//apertura file in scrittura binaria distruttiva
			fclose(ptr);	//chiusura file

			if ((ptr = fopen("user.dat", "rb+")) == NULL) {	//	//apertura file in scrittura binaria
				printf("ERRORE FILE.\n");
			} else {

				i = 0;
				j = 0;

				while (i != 1) {	//cicla fino a controllo != 1

					system("cls");
					printf("\n	SPOTIFY - Aggiunta Utente\n\n");
					printf("Inserire Username.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						uguale = 0;

						k = 0;
						while (utente.username[k]) {	//ciclo azzeramento array
							utente.username[k] = 0;
							k++;
						}

						fflush(stdin);
						gets(utente.username);	//acquisizione username utente

						k = 0;
						while (utente.username[k]) {	//conversione array per scrittura in file, converte ogni posizione
							utente.username[k] = conversione(
									utente.username[k]);
							k++;
						}

						if (utente.username[25] != 0)//controllo lunghezza array non superiore a 25
							//se posizione 25 occupata stampa errore
							printf("ERRORE. IL CAMPO USERNAME NON PUO' SUPERARE I 25 CARATTERI.\n");

						k = 0;

						rewind(ptr);												//posiziona puntatore all'inizio file
						while (!feof(ptr)) {										//cicla puntatore fino a fine file
							fseek(ptr, k * sizeof(utente), SEEK_SET);				//posiziona puntatore a struct in posizione k
							fread(&utentetemp, sizeof(utente), 1, ptr);				//carica in memoria la struct utente temporaneo
							if (!strcmp(utentetemp.username, utente.username)) {	//confronto utnetetemp.username con utente.username
								printf("!!!USERNAME GIA' ESISTENTE!!!\n"); 			//se uguale stampa messaggio e incrementa variabile uguale
								uguale = 1;
								break;
							}
							k++;
						}
						if (uguale == 0 && utente.username[25] == 0)	//se condizioni soddisfatte esci dal ciclo
							n++;
					}

					printf("\nVuoi generare la password o inserirla manualmente?\nDigita 1 per generarla o digita 0 per inserirla manualmente.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						scanf("%d", &gen);	//acquisizione input

						if (gen == 1 || gen == 0)
							n++; //se gen == 0 o 1 esci dal ciclo

						else
							printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
					}

					if (gen == 1) { //se input ==1 genera password, altrimenti continua con input manuale password

						printf("\nInserire livello sicurezza password.\n1) Poco sicura	2) Mediamente sicura	3) Molto sicura.\n");

						n = 0;
						while (n != 1) { //ciclo for per controllo input, se condizioni soddisfatte esci dal ciclo

							contnum = 0; //reset contatore numeri
							contspec = 0; //reset contatore caratteri speciali
							pass = 0; //reset contatore password
							contpass = 0; //reset contatore numeri e caratteri speciali password

							k = 0;
							while (utente.password[k]) {
								utente.password[k] = 0;
								k++;
							}

							scanf("%d", &ins); //prende in input valore per livello sicurezza password

							if (ins == 1 || ins == 2 || ins == 3) {

								Generatore(ins); //chiamata funzione Generatore
								controlloPass(); //chiamata funzione controllo password
								contpass = contnum + contspec; //somma contatore numeri e caratteri speciali
								sicurezzaPass(contpass); //chiamata funzione controllo sicurezza
								printf("\n");
								n++;

							} else
								printf("Errore. Inserire un numero tra 1 e 3.\n");
						}

					} else {

						printf("\nInserire password.\nLa password deve essere di 8 caratteri e deve contenere almeno un numero, un carattere speciale e deve inziare con la lettera maiuscola.\n");
						n = 0;
						while (n != 1) { //ciclo per determinare correttezza password

							contnum = 0; //reset contatore numeri
							contspec = 0; //reset contatore caratteri speciali
							pass = 0; //reset contatore password
							contpass = 0; //reset contatore numeri e caratteri speciali password

							k = 0;
							while (utente.password[k]) {
								utente.password[k] = 0;
								k++;
							}

							scanf("%s", utente.password);

							printf("\n");

							controlloPass(); //chiamata funzione controllo password

							if (isupper(utente.password[0]) && contnum != 0
									&& contspec != 0 && utente.password[8] == 0
									&& pass == 0) {
								//se condizioni iniziali soddisfatte entra nell'if e al termine esci dal ciclo, altrimenti ripeti ciclo
								contpass = contnum + contspec; // somma contatore numeri e caratteri speciali

								sicurezzaPass(contpass); //chiamata funzione controllo sicurezza password

								printf("\nVuoi reinserirla? Digita 1 per continuare, 0 per reinserire la password.\n");

								reins = 0;
								while (reins != 1) {	//cicla fino a controllo != 1

									scanf("%d", &s);	//acquisizione input

									if (s == 1 || s == 0)
										reins++; //se reins == 0 o 1 esci dal ciclo

									else
										printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
								}

								if (s == 1)
									n++; //se input ==1 esci dal ciclo
								printf("\n");
							}
						}
					}

					printf("\nInserire Nome Utente.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						k = 0;
						while (utente.nome[k]) {	//ciclo azzeramento array
							utente.nome[k] = 0;
							k++;
						}

						fflush(stdin);
						gets(utente.nome);	//acquisizione nome utente

						if (utente.nome[15] != 0)	//controllo lunghezza array non superiore a 15
							//se posizione 15 occupata stampa errore
							printf("ERRORE. IL CAMPO NOME NON PUO' SUPERARE I 15 CARATTERI.\n");

						k = 0;
						while (utente.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
							utente.nome[k] = conversione(utente.nome[k]);
							k++;
						}

						if (utente.nome[15] == 0)	//se condizioni soddisfatte esci dal ciclo
							n++;
					}

					printf("\nInserire Cognome.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						k = 0;
						while (utente.cognome[k]) {	//ciclo azzeramento array
							utente.cognome[k] = 0;
							k++;
						}

						fflush(stdin);
						gets(utente.cognome);	//acquisizione cognome utente

						if (utente.cognome[15] != 0)	//controllo lunghezza array non superiore a 15
							//se posizione 15 occupata stampa errore
							printf("ERRORE. IL CAMPO COGNOME NON PUO' SUPERARE I 15 CARATTERI.\n");

						k = 0;
						while (utente.cognome[k]) {	//conversione array per scrittura in file, converte ogni posizione
							utente.cognome[k] = conversione(utente.cognome[k]);
							k++;
						}

						if (utente.cognome[15] == 0)	//se condizioni soddisfatte esci dal ciclo
							n++;
					}

					printf("\nInserire Anno di Nascita nel formato GG/MM/AAAA.\n");
					utente.data_nascita = inserimento_data();	//acquisizione data tramite chiamata funzione

					printf("\nInserire Data Iscrizione nel formato GG/MM/AAAA.\n");

					n = 0;
					while (n != 1) {

						utente.data_iscr = inserimento_data();	//acquisizione data tramite chiamata funzione

						if (utente.data_iscr.anno > utente.data_nascita.anno)	//se anno inscrizione > anno nascita esci dal ciclo
							//altrimenti stampa errore
							n++;

						else
							printf("ERRORE. L'ANNO DI ISCRIZIONE NON PUO' ESSERE MINORE DELL'ANNO DI NASCITA\n");
					}

					printf("\n\nVuoi inserire questo utente? Digita 1 per confermare, 0 per uscire.\n");

					n = 0;
					while (n != 1) {	//cicla fino a controllo != 1

						scanf("%d", &s);	//acquisizone input

						if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
							n++;
						else
							printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
					}

					if (s == 0) {	//se s == 0 esci dal ciclo
						printf("\n!!!UTENTE NON AGGIUNTO!!!\n");
						system("pause");
						i++;
					}

					if (s == 1) {	//se s == 1 continua procedura, posiziona puntatore posizione j e scrittura struct nel file

						fseek(ptr, j * sizeof(utente), SEEK_SET);
						fwrite(&utente, sizeof(utente), 1, ptr);

						printf("\n!!!UTENTE AGGIUNTO!!!\n");

						printf("\nDigita 1 per inserire i dati di un altro utente, 0 per tornare al menu.\n");

						n = 0;
						while (n != 1) {	//cicla fino a controllo != 1

							scanf("%d", &s);	//acquisizone input

							if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
								n++;
							else
								printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
						}

						if (s == 0) {	//se s == 0 esci dal ciclo
							i++;
						}
						j++;	//aumenta contatore indirizzi
					}
				}
				fclose(ptr);	//chiusura del file
			}
			controllo = 0;	//azzeramento variabile controllo
		}
	}
	return 0;

}

int aggiunta_utenti(FILE *ptr) {	//funzione aggiunta utenti

	i = 0;
	s = 0;
	j = 0;

	if ((ptr = fopen("user.dat", "rb+")) == NULL) {	//apertura file in scrittura binaria
		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&utente, sizeof(utente), 1, ptr);		//carica in memoria struct utenti e aumenta contatore indirizzi
			j++;
		}

		j--;	//sottrae di 1 contatore indirizzi per indicare correttamente ultimo indirizzo nel file

		while (i != 1) {	//cicla fino a controllo != 1

			system("cls");
			printf("\n	SPOTIFY - Aggiunta Utente\n\n");
			printf("Inserire Username.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				uguale = 0;

				k = 0;
				while (utente.username[k]) {	//ciclo azzeramento array
					utente.username[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(utente.username);	//acquisizione username utente

				k = 0;
				while (utente.username[k]) {	//conversione array per scrittura in file, converte ogni posizione
					utente.username[k] = conversione(utente.username[k]);
					k++;
				}

				if (utente.username[25] != 0)	//controllo lunghezza array non superiore a 25
					//se posizione 25 occupata stampa errore
					printf("ERRORE. IL CAMPO USERNAME NON PUO' SUPERARE I 25 CARATTERI.\n");

				k = 0;

				rewind(ptr);												//posiziona puntatore all'inizio file
				while (!feof(ptr)) {										//cicla puntatore fino a fine file
					fseek(ptr, k * sizeof(utente), SEEK_SET);				//posiziona puntatore a struct in posizione k
					fread(&utentetemp, sizeof(utente), 1, ptr);				//carica in memoria la struct utente temporaneo
					if (!strcmp(utentetemp.username, utente.username)) {	//confronto utnetetemp.username con utente.username
						printf("!!!USERNAME GIA' ESISTENTE!!!\n"); 			//se uguale stampa messaggio e incrementa variabile uguale
						uguale = 1;
						break;
					}
					k++;
				}
				if (uguale == 0 && utente.username[25] == 0)	//se condizioni soddisfatte esci dal ciclo
					n++;
			}

			printf("\nVuoi generare la password o inserirla manualmente?\nDigita 1 per generarla o digita 0 per inserirla manualmente.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				scanf("%d", &gen);	//acquisizione input

				if (gen == 1 || gen == 0)
					n++; //se gen == 0 o 1 esci dal ciclo

				else
					printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
			}

			if (gen == 1) {	//se input ==1 genera password, altrimenti continua con input manuale password

				printf("\nInserire livello sicurezza password.\n1) Poco sicura	2) Mediamente sicura	3) Molto sicura.\n");

				n = 0;
				while (n != 1) { //ciclo for per controllo input, se condizioni soddisfatte esci dal ciclo

					contnum = 0; //reset contatore numeri
					contspec = 0; //reset contatore caratteri speciali
					pass = 0; //reset contatore password
					contpass = 0; //reset contatore numeri e caratteri speciali password

					k = 0;
					while (utente.password[k]) {
						utente.password[k] = 0;
						k++;
					}

					scanf("%d", &ins); //prende in input valore per livello sicurezza password

					if (ins == 1 || ins == 2 || ins == 3) {
						Generatore(ins); //chiamata funzione Generatore
						controlloPass(); //chiamata funzione controllo password
						contpass = contnum + contspec; //somma contatore numeri e caratteri speciali
						sicurezzaPass(contpass); //chiamata funzione controllo sicurezza
						printf("\n");
						n++;

					} else
						printf("Errore. Inserire un numero tra 1 e 3.\n");
				}

			} else {
				printf("\nInserire password.\nLa password deve essere di 8 caratteri e deve contenere almeno un numero, un carattere speciale e deve inziare con la lettera maiuscola.\n");

				n = 0;
				while (n != 1) { //ciclo per determinare correttezza password

					contnum = 0; //reset contatore numeri
					contspec = 0; //reset contatore caratteri speciali
					pass = 0; //reset contatore password
					contpass = 0; //reset contatore numeri e caratteri speciali password

					k = 0;
					while (utente.password[k]) {
						utente.password[k] = 0;
						k++;
					}

					scanf("%s", utente.password);

					printf("\n");

					controlloPass(); //chiamata funzione controllo password

					if (isupper(utente.password[0]) && contnum != 0
							&& contspec != 0 && utente.password[8] == 0
							&& pass == 0) {
						//se condizioni iniziali soddisfatte entra nell'if e al termine esci dal ciclo, altrimenti ripeti ciclo
						contpass = contnum + contspec; //somma contatore numeri e caratteri speciali

						sicurezzaPass(contpass); //chiamata funzione controllo sicurezza password

						printf("\nVuoi reinserirla? Digita 1 per continuare, 0 per reinserire la password.\n");

						reins = 0;
						while (reins != 1) {	//cicla fino a controllo != 1

							scanf("%d", &s);	//acquisizione input

							if (s == 1 || s == 0)
								reins++; //se reins == 0 o 1 esci dal ciclo

							else
								printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
						}

						if (s != 1)
							n++; //se input == 1 ricicla per reinserimento password
						printf("\n");
					}
				}
			}

			printf("\nInserire Nome Utente.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				k = 0;
				while (utente.nome[k]) {	//ciclo azzeramento array
					utente.nome[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(utente.nome);	//acquisizione nome utente

				if (utente.nome[15] != 0)	//controllo lunghezza array non superiore a 15
					//se posizione 15 occupata stampa errore
					printf("ERRORE. IL CAMPO NOME NON PUO' SUPERARE I 15 CARATTERI.\n");

				k = 0;
				while (utente.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
					utente.nome[k] = conversione(utente.nome[k]);
					k++;
				}

				if (utente.nome[15] == 0)	//se condizioni soddisfatte esci dal ciclo
					n++;
			}

			printf("\nInserire Cognome.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				k = 0;
				while (utente.cognome[k]) {	//ciclo azzeramento array
					utente.cognome[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(utente.cognome);	//acquisizione cognome utente

				if (utente.cognome[15] != 0)	//controllo lunghezza array non superiore a 15
					//se posizione 15 occupata stampa errore
					printf("ERRORE. IL CAMPO COGNOME NON PUO' SUPERARE I 15 CARATTERI.\n");

				k = 0;
				while (utente.cognome[k]) {	//conversione array per scrittura in file, converte ogni posizione
					utente.cognome[k] = conversione(utente.cognome[k]);
					k++;
				}

				if (utente.cognome[15] == 0)	//se condizioni soddisfatte esci dal ciclo
					n++;
			}

			printf("\nInserire Anno di Nascita nel formato GG/MM/AAAA.\n");
			utente.data_nascita = inserimento_data();	//acquisizione data tramite chiamata funzione

			printf("\nInserire Data Iscrizione nel formato GG/MM/AAAA.\n");

			n = 0;
			while (n != 1) {

				utente.data_iscr = inserimento_data();	//acquisizione data tramite chiamata funzione

				if (utente.data_iscr.anno > utente.data_nascita.anno)	//se anno inscrizione > anno nascita esci dal ciclo
					//altrimenti stampa errore
					n++;

				else
					printf("ERRORE. L'ANNO DI ISCRIZIONE NON PUO' ESSERE MINORE DELL'ANNO DI NASCITA\n");
			}

			printf("\n\nVuoi inserire questo utente? Digita 1 per confermare, 0 per uscire.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				scanf("%d", &s);	//acquisizone input

				if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
					n++;
				else
					printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
			}

			if (s == 0) {	//se s == 0 esci dal ciclo
				printf("\n!!!UTENTE NON AGGIUNTO!!!\n");
				system("pause");
				i++;
			}

			if (s == 1) {	//se s == 1 continua procedura, posiziona puntatore posizione j e scrittura struct nel file

				fseek(ptr, j * sizeof(utente), SEEK_SET);
				fwrite(&utente, sizeof(utente), 1, ptr);

				printf("\n!!!UTENTE AGGIUNTO!!!\n");

				printf("\nDigita 1 per inserire i dati di un altro utente, 0 per tornare al menu.\n");

				n = 0;
				while (n != 1) {	//cicla fino a controllo != 1

					scanf("%d", &s);	//acquisizone input

					if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
						n++;
					else
						printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
				}

				if (s == 0) {	//se s == 0 esci dal ciclo
					i++;
				}
				j++;	//aumenta contatore indirizzi
			}
		}
		fclose(ptr);	//chiusura del file
	}
	return 0;

}

int visualizzazione_utenti(FILE *ptr) {	//funzione visualizzazione utenti presenti

	i = 0;
	j = 0;
	user utente = { 0 };

	if ((ptr = fopen("user.dat", "rb")) == NULL) { //apertura file in lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&utente, sizeof(utente), 1, ptr);		//carica in memoria struct utenti e aumenta contatore indirizzi
			j++;
		}

		j--;	//sottrae di 1 contatore indirizzi per indicare correttamente ultimo indirizzo nel file

		system("cls");
		printf("\n	SPOTIFY - Visualizzazione Artisti\n\n");
		printf("Sono presenti %d utenti nel database.\n\n", j);

		for (i = 0; i < j; i++) {	//ciclo for da 0 a numero indirizzi, con posizionamento del puntatore
			//in posizione i e caricamento in memoria della struct utente, con
			//successiva visualizzazione

			fseek(ptr, i * sizeof(utente), SEEK_SET);
			fread(&utente, sizeof(utente), 1, ptr);

			printf("USERNAME: %s\nPASSWORD: %s\nNOME UTENTE: %s\nCOGNOME: %s\nDATA DI NASCITA: %d/%d/%d\nDATA DI ISCRIZIONE: %d/%d/%d\n\n",
					utente.username, utente.password, utente.nome,
					utente.cognome, utente.data_nascita.giorno,
					utente.data_nascita.mese, utente.data_nascita.anno,
					utente.data_iscr.giorno, utente.data_iscr.mese,
					utente.data_iscr.anno);
		}
	}

	fclose(ptr);	//chiusura del file

	system("pause");
	return 0;

}

int modifica_utenti(FILE *ptr) {	//funzione modifica utenti esistenti

	int trovato;

	if ((ptr = fopen("user.dat", "rb")) == NULL) {	//apertura file in lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		j = 0;
		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&utente, sizeof(utente), 1, ptr);		//carica in memoria struct utenti e aumenta contatore indirizzi
			j++;
		}

		j--;	//sottrae di 1 contatore indirizzi per indicare correttamente ultimo indirizzo nel file

		system("cls");
		printf("\n	SPOTIFY - Eliminazione Utente\n\n");

		printf("\nInserire Username dell'utente da eliminare.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			trovato = 0;

			k = 0;
			while (utente.username[k]) {	//ciclo azzeramento array
				utente.username[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(utente.username);	//acquisizione username utente

			k = 0;
			while (utente.username[k]) {	//conversione array per scrittura in file, converte ogni posizione
				utente.username[k] = conversione(utente.username[k]);
				k++;
			}

			z = 0;

			rewind(ptr);												//posiziona puntatore all'inizio file
			while (!feof(ptr)) {										//cicla puntatore fino a fine file
				fseek(ptr, z * sizeof(utente), SEEK_SET);				//posiziona puntatore a struct in posizione z
				fread(&utentetemp, sizeof(utente), 1, ptr);				//carica in memoria la struct utente temporaneo
				if (!strcmp(utentetemp.username, utente.username)) {	//confronto utnetetemp.username con utente.username
					printf("\n!!!UTENTE TROVATO!!!\n"); 				//se uguale stampa messaggio e incrementa variabile trovato
					trovato = 1;
					break;
				}
				z++;
			}

			if (trovato == 1)	//se variabile trovato == 1 esci dal ciclo, altrimenti stampa errore
				n++;
			else
				printf("\n!!!UTENTE NON TROVATO!!!\n");
		}

	}
	fclose(ptr);	//chiusura del file

	ptr = fopen("tmp.txt", "wb");	//apertura file in scrittura binaria distruttiva
	fclose(ptr);	//chiusura del file

	for (i = 0; i < z; i++) {	//ciclo for da indirizzo 0 a indirizzo z, con apertura del file user
		//in modalit� lettura, caricamento della struct in memoria, chiusura del file
		//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria
		//e chiusura del file

		if ((ptr = fopen("user.dat", "rb")) == NULL) {
			printf("ERRORE FILE.\n");

		} else {

			fseek(ptr, i * sizeof(utente), SEEK_SET);
			fread(&utente, sizeof(utente), 1, ptr);
		}
		fclose(ptr);

		if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
			printf("ERRORE FILE.\n");
		} else {
			fseek(ptr, i * sizeof(utente), SEEK_SET);
			fwrite(&utente, sizeof(utente), 1, ptr);
		}
		fclose(ptr);
	}

	printf("\nInserire i Dati del Nuovo Utente\n");

	printf("\nInserire Username.\n");

	if ((ptr = fopen("user.dat", "rb")) == NULL) {	//apertura file in lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			uguale = 0;

			k = 0;
			while (utente.username[k]) {	//ciclo azzeramento array
				utente.username[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(utente.username);	//acquisizione username utente

			k = 0;
			while (utente.username[k]) {	//conversione array per scrittura in file, converte ogni posizione
				utente.username[k] = conversione(utente.username[k]);
				k++;
			}

			if (utente.username[25] != 0)	//controllo lunghezza array non superiore a 25
				//se posizione 25 occupata stampa errore
				printf("ERRORE. IL CAMPO USERNAME NON PUO' SUPERARE I 25 CARATTERI.\n");

			k = 0;

			rewind(ptr);												//posiziona puntatore all'inizio file
			while (!feof(ptr)) {										//cicla puntatore fino a fine file
				fseek(ptr, k * sizeof(utente), SEEK_SET);				//posiziona puntatore a struct in posizione k
				fread(&utentetemp, sizeof(utente), 1, ptr);				//carica in memoria la struct utente temporaneo
				if (!strcmp(utentetemp.username, utente.username)) {	//confronto utnetetemp.username con utente.username
					printf("!!!USERNAME GIA' ESISTENTE!!!\n"); 			//se uguale stampa messaggio e incrementa variabile uguale
					uguale = 1;
					break;
				}
				k++;
			}
			if (uguale == 0 && utente.username[25] == 0)	//se condizioni soddisfatte esci dal ciclo
				n++;
		}
	}
	fclose(ptr);	//chiusura del file


	printf("\nVuoi generare la password o inserirla manualmente?\nDigita 1 per generarla o digita 0 per inserirla manualmente.\n");

	n = 0;
	while (n != 1) {	//cicla fino a controllo != 1

		scanf("%d", &gen);	//acquisizione input

		if (gen == 1 || gen == 0)
			n++; //se gen == 0 o 1 esci dal ciclo

		else
			printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
	}

	if (gen == 1) { //se input == 1 genera password, altrimenti continua con input manuale password

		printf("\nInserire livello sicurezza password.\n1) Poco sicura	2) Mediamente sicura	3) Molto sicura.\n");

		n = 0;
		while (n != 1) { //ciclo for per controllo input, se condizioni soddisfatte esci dal ciclo

			contnum = 0; //reset contatore numeri
			contspec = 0; //reset contatore caratteri speciali
			pass = 0; //reset contatore password
			contpass = 0; //reset contatore numeri e caratteri speciali password

			k = 0;
			while (utente.password[k]) {
				utente.password[k] = 0;
				k++;
			}

			scanf("%d", &ins); //prende in input valore per livello sicurezza password
			if (ins == 1 || ins == 2 || ins == 3) {
				Generatore(ins); //chiamata funzione Generatore
				controlloPass(); //chiamata funzione controllo password
				contpass = contnum + contspec; //somma contatore numeri e caratteri speciali
				sicurezzaPass(contpass); //chiamata funzione controllo sicurezza
				printf("\n");
				n++;

			} else
				printf("Errore. Inserire un numero tra 1 e 3.\n");
		}

	} else {
		printf("\nInserire password.\nLa password deve essere di 8 caratteri e deve contenere almeno un numero, un carattere speciale e deve inziare con la lettera maiuscola.\n");

		n = 0;
		while (n != 1) { // ciclo per determinare correttezza password

			contnum = 0; // reset contatore numeri
			contspec = 0; // reset contatore caratteri speciali
			pass = 0; // reset contatore password
			contpass = 0; // reset contatore numeri e caratteri speciali password

			k = 0;
			while (utente.password[k]) {
				utente.password[k] = 0;
				k++;
			}

			scanf("%s", utente.password);

			printf("\n");

			controlloPass(); // chiamata funzione controllo password
			if (isupper(utente.password[0]) && contnum != 0 && contspec != 0
					&& utente.password[8] == 0 && pass == 0) {
				// se condizioni iniziali soddisfatte entra nell'if e al termine esci dal ciclo, altrimenti ripeti ciclo
				contpass = contnum + contspec; // somma contatore numeri e caratteri speciali

				sicurezzaPass(contpass); // chiamata funzione controllo sicurezza password

				printf("\nVuoi reinserirla? Digita 1 per continuare, 0 per reinserire la password.\n");

				reins = 0;
				while (reins != 1) {	//cicla fino a controllo != 1

					scanf("%d", &s);	//acquisizione input

					if (s == 1 || s == 0)
						reins++; //se reins == 0 o 1 esci dal ciclo

					else
						printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
				}

				if (s != 1)
					n++; // se input ==1 ricicla per reinserimento password
				printf("\n");
			}
		}
	}

	printf("\nInserire Nome Utente.\n");

	n = 0;
	while (n != 1) {	//cicla fino a controllo != 1

		k = 0;
		while (utente.nome[k]) {	//ciclo azzeramento array
			utente.nome[k] = 0;
			k++;
		}

		fflush(stdin);
		gets(utente.nome);	//acquisizione nome utente

		if (utente.nome[15] != 0)	//controllo lunghezza array non superiore a 15
			//se posizione 15 occupata stampa errore
			printf("ERRORE. IL CAMPO NOME NON PUO' SUPERARE I 15 CARATTERI.\n");

		k = 0;
		while (utente.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
			utente.nome[k] = conversione(utente.nome[k]);
			k++;
		}

		if (utente.nome[15] == 0)	//se condizioni soddisfatte esci dal ciclo
			n++;
	}

	printf("\nInserire Cognome.\n");

	n = 0;
	while (n != 1) {	//cicla fino a controllo != 1

		k = 0;
		while (utente.cognome[k]) {	//ciclo azzeramento array
			utente.cognome[k] = 0;
			k++;
		}

		fflush(stdin);
		gets(utente.cognome);	//acquisizione cognome utente

		if (utente.cognome[15] != 0)	//controllo lunghezza array non superiore a 15
			//se posizione 15 occupata stampa errore
			printf("ERRORE. IL CAMPO COGNOME NON PUO' SUPERARE I 15 CARATTERI.\n");

		k = 0;
		while (utente.cognome[k]) {	//conversione array per scrittura in file, converte ogni posizione
			utente.cognome[k] = conversione(utente.cognome[k]);
			k++;
		}

		if (utente.cognome[15] == 0)	//se condizioni soddisfatte esci dal ciclo
			n++;
	}

	printf("\nInserire Anno di Nascita nel formato GG/MM/AAAA.\n");
	utente.data_nascita = inserimento_data();	//acquisizione data tramite chiamata funzione

	printf("\nInserire Data Iscrizione nel formato GG/MM/AAAA.\n");

	n = 0;
	while (n != 1) {	//cicla fino a controllo != 1

		utente.data_iscr = inserimento_data();	//acquisizione data tramite chiamata funzione

		if (utente.data_iscr.anno > utente.data_nascita.anno)	//se anno inscrizione > anno nascita esci dal ciclo
			//altrimenti stampa errore
			n++;

		else
			printf("ERRORE. L'ANNO DI ISCRIZIONE NON PUO' ESSERE MINORE DELL'ANNO DI NASCITA\n");
	}

	printf("\n\nVuoi modificare questo utente? Digita 1 per confermare, 0 per uscire.\n");

	controllo = 0;
	while (controllo != 1) {	//cicla fino a controllo != 1

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			scanf("%d", &s);	//acquisizone input

			if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
				n++;
			else
				printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
		}

		if (s == 0) {	//se s == 0 esci dal ciclo, altrimenti continua procedura
			printf("\n!!!UTENTE NON MODIFICATO!!!\n");
			system("pause");
			controllo = 1;

		} else {

			if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {	//apertura file tmp, con posizionamento puntatore in z
				//e scrittura della struct in memoria nel file
				printf("ERRORE FILE.\n");
			} else {
				fseek(ptr, z * sizeof(utente), SEEK_SET);
				fwrite(&utente, sizeof(utente), 1, ptr);
			}

			fclose(ptr);	//chiusura del file

			z++;	//incremento di 1 contatore indirizzi z

			for (i = z; i < j; i++) {	//ciclo for da indirizzo z a indirizzo j (indirizzi totali),
				//con apertura del file user in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("user.dat", "rb")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fread(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fwrite(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);
			}

			ptr = fopen("user.dat", "wb");	//apertura file in modalit� scrittura distruttiva
			fclose(ptr);	//chiusura del file

			for (i = 0; i < j; i++) {	//ciclo for da indirizzo 0 a indirizzo j (indirizzi totali),
				//con apertura del file tmp in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file user in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {

					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fread(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("user.dat", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fwrite(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);
			}
			printf("\n!!!UTENTE MODIFICATO CON SUCCESSO!!!\n");
			system("pause");
			controllo = 1;	//uscita dal ciclo
		}
	}
	return 0;

}

int eliminazione_utenti(FILE *ptr) {	//funzione modifica utenti esistenti

	int trovato;

	if ((ptr = fopen("user.dat", "rb")) == NULL) {	//apertura file in lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		j = 0;
		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&utente, sizeof(utente), 1, ptr);		//carica in memoria struct utenti e aumenta contatore indirizzi
			j++;
		}

		j--;	//sottrae di 1 contatore indirizzi per indicare correttamente ultimo indirizzo nel file

		n = 0;

		system("cls");
		printf("\n	SPOTIFY - Eliminazione Utente\n\n");

		printf("Inserire Username da Eliminare.\n");

		while (n != 1) {	//cicla fino a controllo != 1

			trovato = 0;

			k = 0;
			while (utente.username[k]) {	//ciclo azzeramento array
				utente.username[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(utente.username);	//acquisizione username utente

			k = 0;
			while (utente.username[k]) {	//conversione array per scrittura in file, converte ogni posizione
				utente.username[k] = conversione(utente.username[k]);
				k++;
			}

			z = 0;

			rewind(ptr);												//posiziona puntatore all'inizio file
			while (!feof(ptr)) {										//cicla puntatore fino a fine file
				fseek(ptr, z * sizeof(utente), SEEK_SET);				//posiziona puntatore a struct in posizione z
				fread(&utentetemp, sizeof(utente), 1, ptr);				//carica in memoria la struct utente temporaneo
				if (!strcmp(utentetemp.username, utente.username)) {	//confronto utnetetemp.username con utente.username
					printf("\n!!!UTENTE TROVATO!!!\n"); 				//se uguale stampa messaggio e incrementa variabile trovato
					trovato = 1;
					break;
				}
				z++;
			}

			if (trovato == 1)	//se variabile trovato == 1 esci dal ciclo, altrimenti stampa errore
				n++;
			else
				printf("\n!!!UTENTE NON TROVATO!!!\n");
		}

	}
	fclose(ptr);	//chiusura del file

	printf("\nVuoi eliminare l'utente?\nDigita 1 per proseguire, 0 per tornare al menu.\n");

	controllo = 0;
	while (controllo != 1) {	//cicla fino a controllo != 1

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			scanf("%d", &s);	//acquisizone input

			if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
				n++;
			else
				printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
		}

		if (s == 0) {	//se s == 0 esci dal ciclo, altrimenti continua procedura
			printf("\n!!!UTENTE NON ELIMINATO!!!\n");
			system("pause");
			controllo = 1;

		} else {

			ptr = fopen("tmp.txt", "wb");	//apertura file in scrittura binaria distruttiva
			fclose(ptr);	//chiusura del file

			for (i = 0; i < z; i++) {	//ciclo for da indirizzo 0 a indirizzo z, con apertura del file user
				//in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("user.dat", "rb")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {

					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fread(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fwrite(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);
			}

			n = z;	//assegna a n valore indirizzo z e incrementa n di 1
			n++;

			for (i = n; i < j; i++) {	//ciclo for da indirizzo n a indirizzo j (indirizzi totali),
				// con apertura del file user in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file tmp in modalit� scrittura binaria, posizionamento del
				//puntatore in z, scrittura della struct in memoria	e chiusura del file

				if ((ptr = fopen("user.dat", "rb")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {

					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fread(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {

					fseek(ptr, z * sizeof(utente), SEEK_SET);
					fwrite(&utente, sizeof(utente), 1, ptr);
				}
				z++;
				fclose(ptr);
			}

			ptr = fopen("user.dat", "wb");	//apertura file in modalit� scrittura distruttiva
			fclose(ptr);	//chiusura del file

			for (i = 0; i < z; i++) {	//ciclo for da indirizzo 0 a indirizzo z (indirizzi totali),
				//con apertura del file tmp in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file user in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {

					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fread(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("user.dat", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");

				} else {

					fseek(ptr, i * sizeof(utente), SEEK_SET);
					fwrite(&utente, sizeof(utente), 1, ptr);
				}
				fclose(ptr);
			}
			printf("\n!!!UTENTE ELIMINATO!!!\n");
			system("pause");
			controllo = 1;	//uscita dal ciclo
		}
	}
	return 0;

}

int login_admin(FILE *ptr) {  //funzione login amministratore

	if ((ptr = fopen("user.dat", "rb")) == NULL) {	//apertura file modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		system("cls");
		printf("\n	SPOTIFY - Login Utente\n\n");
		printf("Inserire Username Utente da gestire.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			uguale = 0;

			k = 0;
			while (preferenza.username[k]) {	//ciclo azzeramento array
				preferenza.username[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(preferenza.username);	//acquisizione username

			k = 0;
			while (preferenza.username[k]) {	//conversione array per scrittura in file, converte ogni posizione
				preferenza.username[k] = conversione(preferenza.username[k]);
				k++;
			}

			k = 0;

			rewind(ptr);													//posiziona puntatore all'inizio file
			while (!feof(ptr)) {											//cicla puntatore fino a fine file
				fseek(ptr, k * sizeof(utente), SEEK_SET);					//posiziona puntatore a struct in posizione k
				fread(&utentetemp, sizeof(utente), 1, ptr);					//carica in memoria la struct utente temporaneo
				if (!strcmp(utentetemp.username, preferenza.username)) {	//confronto utnetetemp.username con preferenza.username
					uguale = 1;												//indirizzo k a id_utente
					id_utente = k;
					break;
				}
				k++;
			}

			if (uguale == 1) {	//se uguale == 1 esci dal ciclo altrimenti stampa errore

				printf("\n!!!UTENTE TROVATO. LOGIN IN CORSO...!!!\n\n");
				system("pause");
				n++;
			}
			else
				printf("\n!!!UTENTE NON TROVATO. REINSERIRE USERNAME!!!\n");
		}
	}
	fclose(ptr);

	return 0;

}

int login(FILE *ptr) {	//funzione login utente

	if ((ptr = fopen("user.dat", "rb")) == NULL) {	//apertura file modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		system("cls");
		printf("\n	SPOTIFY - Login\n\n");

		printf("Inserire Username.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			uguale = 0;

			k = 0;
			while (preferenza.username[k]) {	//ciclo azzeramento array
				preferenza.username[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(preferenza.username);	//acquisizione username

			k = 0;
			while (preferenza.username[k]) {	//conversione array per scrittura in file, converte ogni posizione
				preferenza.username[k] = conversione(preferenza.username[k]);
				k++;
			}


			k = 0;

			rewind(ptr);													//posiziona puntatore all'inizio file
			while (!feof(ptr)) {											//cicla puntatore fino a fine file
				fseek(ptr, k * sizeof(utente), SEEK_SET);					//posiziona puntatore a struct in posizione k
				fread(&utentetemp, sizeof(utente), 1, ptr);					//carica in memoria la struct utente temporaneo
				if (!strcmp(utentetemp.username, preferenza.username)) {	//confronto utnetetemp.username con preferenza.username
					uguale = 1;												//indirizzo k a id_utente
					id_utente = k;
					break;
				}
				k++;
			}

			if (uguale == 1)	//se uguale == 1 esci dal ciclo altrimenti stampa errore
				n++;
			else
				printf("\n!!!UTENTE NON TROVATO. REINSERIRE USERNAME!!!\n");
		}

		printf("\nInserire Password.\n");

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			uguale = 0;

			k = 0;
			while (utente.password[k]) {	//ciclo azzeramento array
				utente.password[k] = 0;
				k++;
			}

			fflush(stdin);
			gets(utente.password);	//acquisizione password

			if (!strcmp(utentetemp.password, utente.password)) {	//confronto tra password in memoria e password inserita
				printf("\nPASSWORD CORRETTA. LOGIN IN CORSO...\n");	//se corretta incrementa contatore uguale
				system("pause");
				uguale = 1;
			}

			if (uguale == 1)	//se uguale == 1 esci dal ciclo, altrimenti stampa errore
				n++;
			else
				printf("\nPASSWORD ERRATA. REINSERIRE PASSWORD\n");
		}
	}

	return 0;

}

int registrazione(FILE *ptr) {	//funzione registrazione utente

	i = 0;
	s = 0;
	j = 0;

	if ((ptr = fopen("user.dat", "rb+")) == NULL) {	//apertura file in scrittura binaria
		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&utente, sizeof(utente), 1, ptr);		//carica in memoria struct utenti e aumenta contatore indirizzi
			j++;
		}

		j--;	//sottrae di 1 contatore indirizzi per indicare correttamente ultimo indirizzo nel file

		while (i != 1) {	//cicla fino a controllo != 1

			system("cls");
			printf("\n	SPOTIFY - Registrazione\n\n");
			printf("Inserire Username.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				uguale = 0;

				k = 0;
				while (utente.username[k]) {	//ciclo azzeramento array
					utente.username[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(utente.username);	//acquisizione username utente

				k = 0;
				while (utente.username[k]) {	//conversione array per scrittura in file, converte ogni posizione
					utente.username[k] = conversione(utente.username[k]);
					k++;
				}

				if (utente.username[25] != 0)	//controllo lunghezza array non superiore a 25
					//se posizione 25 occupata stampa errore
					printf("ERRORE. IL CAMPO USERNAME NON PUO' SUPERARE I 25 CARATTERI.\n");

				k = 0;

				rewind(ptr);												//posiziona puntatore all'inizio file
				while (!feof(ptr)) {										//cicla puntatore fino a fine file
					fseek(ptr, k * sizeof(utente), SEEK_SET);				//posiziona puntatore a struct in posizione k
					fread(&utentetemp, sizeof(utente), 1, ptr);				//carica in memoria la struct utente temporaneo
					if (!strcmp(utentetemp.username, utente.username)) {	//confronto utnetetemp.username con utente.username
						printf("!!!USERNAME GIA' ESISTENTE!!!\n"); 			//se uguale stampa messaggio e incrementa variabile uguale
						uguale = 1;
						break;
					}
					k++;
				}
				if (uguale == 0 && utente.username[25] == 0)	//se condizioni soddisfatte esci dal ciclo
					n++;
			}

			printf("\nVuoi generare la password o inserirla manualmente?\nDigita 1 per generarla o digita 0 per inserirla manualmente.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				scanf("%d", &gen);	//acquisizione input

				if (gen == 1 || gen == 0)
					n++; //se gen == 0 o 1 esci dal ciclo

				else
					printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
			}

			if (gen == 1) {	//se input ==1 genera password, altrimenti continua con input manuale password

				printf("\nInserire livello sicurezza password.\n1) Poco sicura	2) Mediamente sicura	3) Molto sicura.\n");

				n = 0;
				while (n != 1) { //ciclo for per controllo input, se condizioni soddisfatte esci dal ciclo

					contnum = 0; //reset contatore numeri
					contspec = 0; //reset contatore caratteri speciali
					pass = 0; //reset contatore password
					contpass = 0; //reset contatore numeri e caratteri speciali password

					k = 0;
					while (utente.password[k]) {
						utente.password[k] = 0;
						k++;
					}

					scanf("%d", &ins); //prende in input valore per livello sicurezza password

					if (ins == 1 || ins == 2 || ins == 3) {
						Generatore(ins); //chiamata funzione Generatore
						controlloPass(); //chiamata funzione controllo password
						contpass = contnum + contspec; //somma contatore numeri e caratteri speciali
						sicurezzaPass(contpass); //chiamata funzione controllo sicurezza
						printf("\n");
						n++;

					} else
						printf("Errore. Inserire un numero tra 1 e 3.\n");
				}

			} else {
				printf("\nInserire password.\nLa password deve essere di 8 caratteri e deve contenere almeno un numero, un carattere speciale e deve inziare con la lettera maiuscola.\n");

				n = 0;
				while (n != 1) { //ciclo per determinare correttezza password

					contnum = 0; //reset contatore numeri
					contspec = 0; //reset contatore caratteri speciali
					pass = 0; //reset contatore password
					contpass = 0; //reset contatore numeri e caratteri speciali password

					k = 0;
					while (utente.password[k]) {
						utente.password[k] = 0;
						k++;
					}

					scanf("%s", utente.password);

					printf("\n");

					controlloPass(); //chiamata funzione controllo password

					if (isupper(utente.password[0]) && contnum != 0
							&& contspec != 0 && utente.password[8] == 0
							&& pass == 0) {
						//se condizioni iniziali soddisfatte entra nell'if e al termine esci dal ciclo, altrimenti ripeti ciclo
						contpass = contnum + contspec; //somma contatore numeri e caratteri speciali

						sicurezzaPass(contpass); //chiamata funzione controllo sicurezza password

						printf("\nVuoi reinserirla? Digita 1 per continuare, 0 per reinserire la password.\n");

						reins = 0;
						while (reins != 1) {	//cicla fino a controllo != 1

							scanf("%d", &s);	//acquisizione input

							if (s == 1 || s == 0)
								reins++; //se reins == 0 o 1 esci dal ciclo

							else
								printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
						}

						if (s != 1)
							n++; //se input == 1 ricicla per reinserimento password
						printf("\n");
					}
				}
			}

			printf("\nInserire Nome Utente.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				k = 0;
				while (utente.nome[k]) {	//ciclo azzeramento array
					utente.nome[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(utente.nome);	//acquisizione nome utente

				if (utente.nome[15] != 0)	//controllo lunghezza array non superiore a 15
					//se posizione 15 occupata stampa errore
					printf("ERRORE. IL CAMPO NOME NON PUO' SUPERARE I 15 CARATTERI.\n");

				k = 0;
				while (utente.nome[k]) {	//conversione array per scrittura in file, converte ogni posizione
					utente.nome[k] = conversione(utente.nome[k]);
					k++;
				}

				if (utente.nome[15] == 0)	//se condizioni soddisfatte esci dal ciclo
					n++;
			}

			printf("\nInserire Cognome.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				k = 0;
				while (utente.cognome[k]) {	//ciclo azzeramento array
					utente.cognome[k] = 0;
					k++;
				}

				fflush(stdin);
				gets(utente.cognome);	//acquisizione cognome utente

				if (utente.cognome[15] != 0)	//controllo lunghezza array non superiore a 15
					//se posizione 15 occupata stampa errore
					printf("ERRORE. IL CAMPO COGNOME NON PUO' SUPERARE I 15 CARATTERI.\n");

				k = 0;
				while (utente.cognome[k]) {	//conversione array per scrittura in file, converte ogni posizione
					utente.cognome[k] = conversione(utente.cognome[k]);
					k++;
				}

				if (utente.cognome[15] == 0)	//se condizioni soddisfatte esci dal ciclo
					n++;
			}

			printf("\nInserire Anno di Nascita nel formato GG/MM/AAAA.\n");
			utente.data_nascita = inserimento_data();	//acquisizione data tramite chiamata funzione

			printf("\nInserire Data Iscrizione nel formato GG/MM/AAAA.\n");

			n = 0;
			while (n != 1) {

				utente.data_iscr = inserimento_data();	//acquisizione data tramite chiamata funzione

				if (utente.data_iscr.anno > utente.data_nascita.anno)	//se anno inscrizione > anno nascita esci dal ciclo
					//altrimenti stampa errore
					n++;

				else
					printf("ERRORE. L'ANNO DI ISCRIZIONE NON PUO' ESSERE MINORE DELL'ANNO DI NASCITA\n");
			}

			printf("\n\nVuoi inserire questo utente? Digita 1 per confermare, 0 per uscire.\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				scanf("%d", &s);	//acquisizone input

				if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
					n++;
				else
					printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
			}

			if (s == 0) {	//se s == 0 esci dal ciclo
				printf("\n!!!UTENTE NON AGGIUNTO!!!\n");
				system("pause");
				i++;
			}

			if (s == 1) {	//se s == 1 continua procedura, posiziona puntatore posizione j e scrittura struct nel file

				fseek(ptr, j * sizeof(utente), SEEK_SET);
				fwrite(&utente, sizeof(utente), 1, ptr);

				printf("\n!!!UTENTE AGGIUNTO!!!\n");
				system("pause");

				i++;
			}
		}
		fclose(ptr);	//chiusura del file
	}

	return 0;

}

int ricerca(FILE *ptr) {	//funzione ricerca

	char search[25] = { 0 };	//dichiarazione e inizializzazione variabile char per ricerca
	int search_num = 0;			//dichiarazione e inizializzazione variabile int per ricerca
	uguale = 0;

	if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&artista, sizeof(artista), 1, ptr);	//carica in memoria struct artista
		}

		j = artista.id_artista;	//assegna a j ultimo id artista

		uguale = 0;

		system("cls");
		printf("\n	SPOTIFY - Ricerca\n\n");

		printf("Inserire Termine di Ricerca.\n");

		fflush(stdin);
		gets(search);	//acquisizione ricerca

		search_num = atoi(search);	//conversione stringa in intero

		printf("\nRISULTATI DI RICERCA PER '%s'\n\n", search);

		k = 0;
		while (search[k]) {	//conversione array per scrittura in file, converte ogni posizione
			search[k] = conversione(search[k]);
			k++;
		}

		printf("\n%-10s %-25s %-15s %-30s %-15s %-10s\n", "ID ARTISTA",
				"NOME ARTISTA", "GENERE", "PRODUTTORE", "NAZIONALITA'",
				"ANNO DI ATTIVITA'");

		z = 0;
		rewind(ptr);									//posiziona puntatore all'inizio file
		for (z = 0; z < j; z++) {						//ciclo for da 0 a j con caricamento struct da
			fseek(ptr, z * sizeof(artista), SEEK_SET);	//da posizione z nel file in memoria e confronto
			fread(&artista, sizeof(artista), 1, ptr);	//ricerca con i vari campi della struct
			if (!strcmp(search, artista.nome)) {		//se corrispondente chiamata funzione risultati
				risultati();
				uguale = 1;
			}

			if (!strcmp(search, artista.genere)) {
				risultati();
				uguale = 1;
			}

			if (!strcmp(search, artista.produttore)) {
				risultati();
				uguale = 1;
			}

			if (!strcmp(search, artista.nazionalita)) {
				risultati();
				uguale = 1;
			}

			if (search_num == artista.anno_attivita) {
				risultati();
				uguale = 1;
			}
		}

		if (uguale == 0)
			printf("\n\n\n!!!NESSUN RISULTATO. PROVARE CON ALTRI TERMINI DI RICERCA!!!\n");

		printf("\n\n");
		system("pause");
	}

	return 0;

}

int Generatore(int ins) {	//funzione Generatore password
	srand(time(NULL));		//seme generatore utilizzando orologio computer
	char lettere[LETTERE] =
			"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; // array per letter
	char speciali[SPECIALI] = "1234567890|$%&/()=?^{[*+]}@#;,:._-<>"; // array numeri e caratteri speciali
	int indice, h;			//dichiarazione e inizializzazione contatori
	int num = 0;			//dichiarazione e inizializzazione contatore numeri

	if (ins == 1) {	//se inserimento valore == 1
		utente.password[0] = lettere[rand() % 25];	//assegna lettera maiuscola a prima posizione vettore

		for (indice = 0; indice < 2;) {	//ciclo for per assegnazione a password di 2 caratteri speciali
			k = 1 + rand() % 7;
			if (utente.password[k] == 0) {	//se la posizione randomizzata � vuota assegna un carattere
				//speciale a quella posizione, e continua il ciclo
				utente.password[k] = speciali[rand() % 35];
				indice++;
			}
		}

		for (k = 1; k <= 7; k++) {	//ciclo for per determinare contatore numeri.
			//Se la posizione � vuota ed � un numero aumenta contatore
			if (utente.password[k] != 0) {
				if (isdigit(utente.password[k])) {
					num++;
				}
			}
		}

		if (num == 2) {	//se contatore ==2, cicla per tutta la password
			//assegnando alla prima posizione non vuota contenente un valore numerico
			//un carattere speciale
			for (k = 1; k <= 7; k++) {
				if (utente.password[k] != 0) {
					if (isdigit(utente.password[k])) {
						utente.password[k] = speciali[26 + rand() % 10];
						k = 7;
					}
				}
			}
		}

		if (num == 0) {	//se contatore ==0 cicla per tutta la password assegnando
			//alla prima posizione non vuota un valore numerico
			for (k = 1; k <= 7; k++) {
				if (utente.password[k] != 0) {
					utente.password[k] = speciali[rand() % 9];
					k = 7;
				}
			}
		}

		for (k = 1; k <= 7; k++) {	//cicla per tutta la password assegnando lettere alle posizioni vuote
			if (utente.password[k] == 0) {
				utente.password[k] = lettere[rand() % 51];
			}
		}
	} // fine dell'if

	if (ins == 2) {									//se inserimento valore uguale a 2
		utente.password[0] = lettere[rand() % 25];	//assegna lettera maiuscola a
		//prima posizione vettore

		indice = 2 + rand() % 3;

		for (k = 0; k < indice;) {	//ciclo for per assegnazione a password di h caratteri speciali
			h = 1 + rand() % 7;
			if (utente.password[h] == 0) {	//se la posizione randomizzata � vuota assegna un carattere
				//speciale a quella posizione, e continua il ciclo
				utente.password[h] = speciali[rand() % 35];
				k++;
			}
		}

		for (k = 1; k <= 7; k++) {	//ciclo for per determinare contatore numeri.
			//Se la posizione � vuota ed � un numero aumenta contatore
			if (utente.password[k] != 0) {
				if (isdigit(utente.password[k])) {
					num++;
				}
			}
		}

		if (num == indice) {	//se contatore == h, cicla per tutta la password
			//assegnando alla prima posizione non vuota contenente un valore numerico
			//un carattere speciale

			for (k = 1; k <= 7; k++) {
				if (utente.password[k] != 0) {
					if (isdigit(utente.password[k])) {
						utente.password[k] = speciali[26 + rand() % 10];
						k = 7;
					}
				}
			}
		}

		if (num == 0) {	//se contatore == 0 cicla per tutta la password assegnando
			//alla prima posizione non vuota un valore numerico
			for (k = 1; k <= 7; k++) {
				if (utente.password[k] != 0) {
					utente.password[k] = speciali[rand() % 9];
					k = 7;
				}
			}
		}

		for (k = 1; k <= 7; k++) {	//cicla per tutta la password assegnando lettere alle posizioni vuote
			if (utente.password[k] == 0) {
				utente.password[k] = lettere[rand() % 51];
			}
		}
	}	//fine dell'if

	if (ins == 3) {									//se inserimento valore uguale a 3
		utente.password[0] = lettere[rand() % 25];	//assegna lettera maiuscola a prima posizione vettore


		indice = 5 + rand() % 3;

		for (k = 0; k < indice;) {	//ciclo for per assegnazione a password di h caratteri speciali
			h = 1 + rand() % 7;
			if (utente.password[h] == 0) {
				utente.password[h] = speciali[rand() % 35];
				k++;
			}
		}

		for (k = 1; k <= 7; k++) {	//ciclo for per determinare contatore numeri.
			//se la posizione � vuota ed � un numero aumenta contatore
			if (utente.password[k] != 0) {
				if (isdigit(utente.password[k])) {
					num++;
				}
			}
		}

		if (num == indice) {	//se contatore == h, cicla per tutta la password
			//assegnando alla prima posizione non vuota contenente un valore numerico
			//un carattere speciale
			for (k = 1; k <= 7; k++) {
				if (utente.password[k] != 0) {
					if (isdigit(utente.password[k])) {
						utente.password[k] = speciali[26 + rand() % 10];
						k = 7;
					}
				}
			}
		}

		if (num == 0) {	//se contatore == 0 cicla per tutta la password assegnando
			//alla prima posizione non vuota un valore numerico
			for (k = 1; k <= 7; k++) {
				if (utente.password[k] != 0) {
					utente.password[k] = speciali[rand() % 9];
					k = 7;
				}
			}
		}

		for (k = 1; k <= 7; k++) {	//cicla per tutta la password assegnando lettere alle posizioni vuote
			if (utente.password[k] == 0) {
				utente.password[k] = lettere[rand() % 51];
			}
		}
	}

	printf("La password e': %s\n",utente.password);
	return 0; //la funzione restituisce 0
}

int controlloPass() {	//funzione controllo input password

	if (utente.password[8] != 0) {	//se posizione 8 vettore � !=0 (quindi password pi�
		//lunga di 8 caratteri) stampa errore
		printf("Errore. La password supera gli 8 caratteri.\n");
	}

	for (k = 0; k <= 7; k++) { //ciclo for per analizzare array password
		if (utente.password[k] == 0) { //se qualche posizione � == 0 passoword != 8 caratteri, aumenta contatore
			pass++;
		}
		if (isdigit(utente.password[k])) { //se � presente un numero aumenta contatore
			contnum++;
		}
		if (isalnum(utente.password[k]) == 0) { //se � presente un carattere speciale aumenta contatore
			contspec++;
		}
	}

	if (pass != 0) { //se contatore passwor != 0 stampa errore
		printf("Errore. La password deve essere di 8 caratteri.\n");
	}
	if (contnum == 0) { //se contatore numeri == 0 stampa errore
		printf("Errore. La password deve contenere almeno un numero.\n");
	}
	if (contspec == 0) { //se contatore speciali == 0 stampa errore
		printf("Errore. La password deve contenere almeno un carattere speciale.\n");
	}
	if (isupper(utente.password[0]) == 0) { //se non presente lettera iniziale maiuscola stampa errore
		printf("Errore. La password deve iniziare con una lettera maiuscola.\n");
	}
	return 0; //la funzione restituisce 0
} //fine della funzione

int sicurezzaPass(int contpass) { //funzione controllo sicurezza password

	printf("\n");

	if (contpass <= 2) {	//se contatore <= 2 assegna a posizione 9 vettore password valore sentinella
		//1 = poco sicura, 2 = mediamente sicura, 3 = sicura
		printf("La password e' poco sicura.");
	}
	if (contpass > 2 && contpass <= 4) { //se contatore compreso tra 3 e 4 assegna a posizione 9 vettore password valore sentinella
		printf("La password e' mediamente sicura.");
	}
	if (contpass > 4) { //se contatore >= 4 assegna a posizione 9 vettore password valore sentinella
		printf("La password e' sicura.");
	}
	return 0; // la funzione restituisce 0
}

int ascolto(FILE *ptr) {	//funzione ascolto utente

	if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		j = 0;
		rewind(ptr);											//posiziona puntatore all'inizio file
		while (!feof(ptr)) {									//cicla puntatore fino a fine file
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);	//carica in memoria struct preferenza e aumenta contatore
			j++;
		}

		j--;	//decrementa contatore di 1 per correggere ultimo indirizzo

	}
	fclose(ptr);	//chiusura del file

	system("cls");
	printf("\n	SPOTIFY - Ascolto\n\n");

	printf("Inserire Artista da Ascoltare\n");

	n = 0;
	while (n != 1) {	//cicla fino a controllo != 1

		uguale = 0;

		k = 0;
		while (preferenza.artista[k]) {	//ciclo azzeramento array
			preferenza.artista[k] = 0;
			k++;
		}

		fflush(stdin);
		gets(preferenza.artista);	//acquisizione nome artista

		k = 0;
		while (preferenza.artista[k]) {	//conversione array per scrittura in file, converte ogni posizione
			preferenza.artista[k] = conversione(preferenza.artista[k]);
			k++;
		}

		if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
			printf("ERRORE FILE.\n");

		} else {

			k = 0;

			rewind(ptr);												//posiziona puntatore all'inizio file
			while (!feof(ptr)) {										//cicla puntatore fino a fine file
				fseek(ptr, k * sizeof(artista), SEEK_SET);				//posiziona puntatore a indirizzo k e
				fread(&artistatemp, sizeof(artista), 1, ptr);			//carica struct in memoria
				if (!strcmp(artistatemp.nome, preferenza.artista)) {	//compara campo in memoria con inserimento
					printf("\nARTISTA TROVATO! Ascolto...\n");			//se uguale stampa messaggio e esce dal ciclo
					uguale = 1;
					break;
				}
				k++;
			}

			if (uguale == 1) {	//se uguale == 1 esce dal ciclo altrimenti stamap errore
				n++;
				system("pause");
			} else
				printf("\n!!!ARTISTA NON TROVATO. RIPROVARE.\n");
		}
		fclose(ptr);	//chiusura del file
	}

	system("cls");
	printf("\n	SPOTIFY - Ascolto\n\n");

	strcpy(preferenza.preferenza, listen);	//copia contenuto stringa listen in campo preferenza

	if ((ptr = fopen("preference.dat", "rb+")) == NULL) {	//apertura file in modalit� scrittura binaria
		printf("ERRORE FILE.\n");

	} else {

		fseek(ptr, j * sizeof(preferenza), SEEK_SET);		//posiziona puntatore all'indirizzo j e scrive struct nel file
		fwrite(&preferenza, sizeof(preferenza), 1, ptr);

		k = 0;

		uguale = 0;

		rewind(ptr);															//posiziona puntatore all'inizio file
		while (!feof(ptr)) {													//cicla puntatore fino a fine file
			fseek(ptr, k * sizeof(preferenza), SEEK_SET);						//posiziona puntatore a indirizzo k
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);					//legge struct e controlla se artista
			if (!strcmp(preferenzatemp.username, preferenza.username)			//gi� ascoltato
					&& !strcmp(preferenzatemp.artista, preferenza.artista)		//se condizioni soddisfatte stampa messaggio
					&& (!strcmp(preferenzatemp.preferenza, like)				//incrementa uguale ed esce dal ciclo
							|| !strcmp(preferenzatemp.preferenza, unlike))) {
				printf("\n\nArtista gia' ascoltato. Per modificare la preferenza accedere al menu' apposito.\n");
				uguale = 1;
				system("pause");
				break;
			}
			k++;
		}

		if (uguale == 0) {	//se uguale == 0 continua ciclo

			printf("\nRiproduzione completata. Digita 1 per 'Like', 2 per 'Dislike'\n");

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				scanf("%d", &s);	//acquisizone input

				if (s == 1 || s == 2)	//se input == 1 o 2 esci dal ciclo
					n++;
				else
					printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
			}

			k = 0;
			while (preferenza.preferenza[k]) {	//ciclo azzeramento array
				preferenza.preferenza[k] = 0;
				k++;
			}

			if (s == 1)	//se s == 1 copia contenuto stringa like in campo preferenza
				strcpy(preferenza.preferenza, like);

			if (s == 2)	//se s == 1 copia contenuto stringa unlike in campo preferenza
				strcpy(preferenza.preferenza, unlike);

			j++;	//aumenta contatore indirizzo

			fseek(ptr, j * sizeof(preferenza), SEEK_SET);	//posiziona puntatore a indirizzo j e scrive struct nel file
			fwrite(&preferenza, sizeof(preferenza), 1, ptr);
		}
	}
	fclose(ptr);	//chiusura del file

	return 0;

}

int profilo(FILE *ptr) {  //funziona di mostra profilo

	int cont_listen = 0;
	int cont_like = 0;
	int cont_unlike = 0;

	if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		n = 0;
		rewind(ptr);											//posiziona puntatore all'inizio file
		while (!feof(ptr)) {									//cicla puntatore fino a fine file
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);	//carica in memoria struct preferenza e aumenta contatore
			n++;
		}

		n--;	//decrementa contatore di 1 per correggere ultimo indirizzo

	}
	fclose(ptr);

	if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in scrittura binaria

		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&artista, sizeof(artista), 1, ptr); 	//lettura struct
		}

		j = artista.id_artista;	//salva in j l'ultimo id artista

	}
	fclose(ptr);

	system("cls");
	printf("\n	SPOTIFY - Profilo Utente\n\n");

	printf("\n %-25s %-2s %-2s %-2s\n\n", "NOME ARTISTA", "LISTEN", "LIKE", "UNLIKE");

	for(i=0; i<j; i++) {

		if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in scrittura binaria

			printf("ERRORE FILE.\n");

		} else {

			fseek(ptr, i * sizeof(artista), SEEK_SET);
			fread(&artista, sizeof(artista), 1, ptr);
		}

		fclose(ptr);

		if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
			printf("ERRORE FILE.\n");

		} else {

			cont_listen = 0;
			cont_like = 0;
			cont_unlike = 0;

			k = 0;

			rewind(ptr);														//posiziona puntatore all'inizio file
			for (k=0; k<n; k++) {
				fseek(ptr, k * sizeof(preferenza), SEEK_SET);					//posiziona puntatore a indirizzo k
				fread(&preferenzatemp, sizeof(preferenza), 1, ptr);				//carica in memoria struct e controlla
				if (!strcmp(preferenzatemp.username, preferenza.username)		//se artista gi� ascoltato
						&& !strcmp(preferenzatemp.artista, artista.nome)		//se condizioni soddisfatte incrementa uguale
						&& !strcmp(preferenzatemp.preferenza, listen)) {		//ed esce dal ciclo
					cont_listen++;
				}

				if (!strcmp(preferenzatemp.username, preferenza.username)		//se artista gi� ascoltato
						&& !strcmp(preferenzatemp.artista, artista.nome)		//se condizioni soddisfatte incrementa uguale
						&& !strcmp(preferenzatemp.preferenza, like)) {			//ed esce dal ciclo
					cont_like++;
				}

				if (!strcmp(preferenzatemp.username, preferenza.username)		//se artista gi� ascoltato
						&& !strcmp(preferenzatemp.artista, artista.nome)		//se condizioni soddisfatte incrementa uguale
						&& !strcmp(preferenzatemp.preferenza, unlike)) {		//ed esce dal ciclo
					cont_unlike++;
				}
			}

		} fclose(ptr);

		k = 0;

		if (isalpha(artista.nome[k]))
		{
			artista.nome[k] = toupper(artista.nome[k]);
		}

		while (artista.nome[k]) {
			if (artista.nome[k] == '_')
			{

				artista.nome[k] = ' ';
				k++;
				if (isalpha(artista.nome[k]))
					artista.nome[k] = toupper(artista.nome[k]);

				k--;
			}
			k++;
		}

		printf(" %-25s %-6d %-4d %-5d\n", artista.nome, cont_listen, cont_like, cont_unlike);
	}

	system("pause");

	return 0;

}

int mod_pref_utente(FILE *ptr) {	//funzione modifica preferenza utente

	char pref_utente[7];	//dichiarazione variabile char preferenza utente

	if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		j = 0;
		rewind(ptr);											//posiziona puntatore all'inizio file
		while (!feof(ptr)) {									//cicla puntatore fino a fine file
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);	//carica in memoria struct preferenza e aumenta contatore
			j++;
		}

		j--;	//decrementa contatore di 1 per correggere ultimo indirizzo

	}

	system("cls");
	printf("\n	SPOTIFY - Modifica Preferenza\n\n");

	printf("Inserire Nome Artista Ascoltato per Modificarne la Preferenza.\n");

	uguale = 0;

	k = 0;
	while (preferenza.artista[k]) {	//ciclo azzeramento array
		preferenza.artista[k] = 0;
		k++;
	}

	fflush(stdin);
	gets(preferenza.artista);	//acquisizione nome artista

	k = 0;
	while (preferenza.artista[k]) {	//conversione array per scrittura in file, converte ogni posizione
		preferenza.artista[k] = conversione(preferenza.artista[k]);
		k++;
	}

	printf("%s\n", preferenza.artista);

	if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		k = 0;
		uguale = 0;


		rewind(ptr);														//posiziona puntatore all'inizio file
		while (!feof(ptr)) {												//cicla puntatore fino a fine file
			fseek(ptr, k * sizeof(preferenza), SEEK_SET);					//posiziona puntatore a indirizzo k
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);				//carica in memoria struct e controlla
			if (!strcmp(preferenzatemp.username, preferenza.username)		//se artista gi� ascoltato
					&& !strcmp(preferenzatemp.artista, preferenza.artista)	//se condizioni soddisfatte incrementa uguale
					&& strcmp(preferenzatemp.preferenza, listen)) {			//ed esce dal ciclo
				printf("\n!!!ARTISTA TROVATO!!!\n");
				uguale = 1;
				break;
			}
			k++;
		}

		fclose(ptr);	//chiusura del file

		strcpy(pref_utente, preferenzatemp.preferenza);	//copia contenuto stringa preferenza in pref_utente

		if (uguale == 1) {	//se uguale == 1 continua procedura

			printf("\n\nSi sta per modifcare il %s dell'Artista %s, digita 1 per proseguire, 0 per tornare al menu'.\n",
					preferenzatemp.preferenza, preferenzatemp.artista);

			n = 0;
			while (n != 1) {	//cicla fino a controllo != 1

				scanf("%d", &s);	//acquisizone input

				if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
					n++;
				else
					printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
			}

			if (s == 1) {

				ptr = fopen("tmp.txt", "wb");	//apertura file in modalit� scrittura binaria distruttiva
				fclose(ptr);	//chiusura del file

				for (i = 0; i < k; i++) {	//ciclo for da indirizzo 0 a indirizzo k, con apertura del file preference
					//in modalit� lettura, caricamento della struct in memoria, chiusura del file
					//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria
					//e chiusura del file

					if ((ptr = fopen("preference.dat", "rb")) == NULL) {
						printf("ERRORE FILE.\n");

					} else {
						fseek(ptr, i * sizeof(preferenza), SEEK_SET);
						fread(&preferenzatemp, sizeof(preferenza), 1, ptr);
					}
					fclose(ptr);

					if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
						printf("ERRORE FILE.\n");
					} else {
						fseek(ptr, i * sizeof(preferenza), SEEK_SET);
						fwrite(&preferenzatemp, sizeof(preferenza), 1, ptr);
					}
					fclose(ptr);
				}

				n = k;	//assegna valore indirizzo k a n e incrementa n di 1
				n++;

				for (i = n; i < j; i++) {	//ciclo for da indirizzo n a indirizzo j (indirizzi totali), con apertura del file preference
					//in modalit� lettura, caricamento della struct in memoria, chiusura del file
					//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria in posizione k
					//e chiusura del file

					if ((ptr = fopen("preference.dat", "rb")) == NULL) {
						printf("ERRORE FILE.\n");
					} else {
						fseek(ptr, i * sizeof(preferenza), SEEK_SET);
						fread(&preferenzatemp, sizeof(preferenza), 1, ptr);
					}
					fclose(ptr);

					if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
						printf("ERRORE FILE.\n");
					} else {
						fseek(ptr, k * sizeof(preferenza), SEEK_SET);
						fwrite(&preferenzatemp, sizeof(preferenza), 1, ptr);
					}
					k++;
					fclose(ptr);
				}

				ptr = fopen("preference.dat", "wb");	//apertura file in modalit� scrittura binaria distruttiva
				fclose(ptr);	//chiusura del file

				for (i = 0; i < k; i++) {	//ciclo for da indirizzo 0 a indirizzo k (indirizzi totali),
					//con apertura del file tmp in modalit� lettura, caricamento della struct in memoria, chiusura del file
					//apertura file preference in modalit� scrittura binaria, scrittura della struct in memoria
					//e chiusura del file

					if ((ptr = fopen("tmp.txt", "rb")) == NULL) {
						printf("ERRORE FILE.\n");
					} else {
						fseek(ptr, i * sizeof(preferenza), SEEK_SET);
						fread(&preferenzatemp, sizeof(preferenza), 1, ptr);
					}
					fclose(ptr);

					if ((ptr = fopen("preference.dat", "rb+")) == NULL) {
						printf("ERRORE FILE.\n");
					} else {
						fseek(ptr, i * sizeof(preferenza), SEEK_SET);
						fwrite(&preferenzatemp, sizeof(preferenza), 1, ptr);
					}
					fclose(ptr);
				}


				if ((ptr = fopen("preference.dat", "rb+")) == NULL) {	//apertura file in modalit� scrittura binaria
					printf("ERRORE FILE.\n");

				} else {

					n = 0;
					while (preferenza.preferenza[n]) {	//ciclo azzeramento array
						preferenza.preferenza[n] = 0;
						n++;
					}

					if (!strcmp(pref_utente, like)) {	//pref_utente contiene "like", copia contenuto stringa unlike in campo preferenza
						strcpy(preferenza.preferenza, unlike);
					}

					if (!strcmp(pref_utente, unlike)) {	//pref_utente contiene "unlike", copia contenuto stringa like in campo preferenza
						strcpy(preferenza.preferenza, like);
					}

					fseek(ptr, k * sizeof(preferenza), SEEK_SET);	//posiziona puntatore a indirizzo k e scrittura struct nel file
					fwrite(&preferenza, sizeof(preferenza), 1, ptr);

				}
				fclose(ptr);	//chiusura del file
				printf("\nPreferenza Modificata con Successo!\n");
				system("pause");

			} else {	//se s != 1 stampa messaggio
				printf("\n!!!PREFERENZA NON MODIFICATA!!!\n");
				system("pause");
			}
		} else {	//se uguale != 1 stampa messaggio
			printf("\n!!!ARTISTA NON TROVATO!!!\n");
			system("pause");
		}

	}

	return 0;

}

int del_pref_utente(FILE *ptr) {	//funzione eliminazione preferenza utente

	char pref_utente[7];	//dichiarazione variabile char preferenza utente

	if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		j = 0;
		rewind(ptr);											//posiziona puntatore all'inizio file
		while (!feof(ptr)) {									//cicla puntatore fino a fine file
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);	//carica in memoria struct preferenza e aumenta contatore
			j++;
		}

		j--;	//decrementa contatore di 1 per correggere ultimo indirizzo

		system("cls");
		printf("\n	SPOTIFY - Elimina Preferenza\n\n");

		printf("Inserire Nome Artista Ascoltato per Eliminarne la Preferenza.\n");

		uguale = 0;

		k = 0;
		while (preferenza.artista[k]) {	//ciclo azzeramento array
			preferenza.artista[k] = 0;
			k++;
		}

		fflush(stdin);
		gets(preferenza.artista);	//acquisizione nome artista

		k = 0;
		while (preferenza.artista[k]) {	//conversione array per scrittura in file, converte ogni posizione
			preferenza.artista[k] = conversione(preferenza.artista[k]);
			k++;
		}

		printf("%s\n", preferenza.artista);

		k = 0;
		uguale = 0;


		rewind(ptr);														//posiziona puntatore all'inizio file
		while (!feof(ptr)) {												//cicla puntatore fino a fine file
			fseek(ptr, k * sizeof(preferenza), SEEK_SET);					//posiziona puntatore a indirizzo k
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);				//carica in memoria struct e controlla
			if (!strcmp(preferenzatemp.username, preferenza.username)		//se artista gi� ascoltato
					&& !strcmp(preferenzatemp.artista, preferenza.artista)	//se condizioni soddisfatte incrementa uguale
					&& strcmp(preferenzatemp.preferenza, listen)) {			//ed esce dal ciclo
				printf("\n!!!ARTISTA TROVATO!!!\n");
				uguale = 1;
				break;
			}
			k++;
		}
	}
	fclose(ptr);	//chiusura del file

	strcpy(pref_utente, preferenzatemp.preferenza);	//copia contenuto stringa preferenza in pref_utente

	if (uguale == 1) {	//se uguale == 1 continua procedura

		printf("\nSi sta per eliminare il %s dell'Artista %s, digita 1 per proseguire, 0 per tornare al menu'.\n",
				preferenzatemp.preferenza, preferenzatemp.artista);

		n = 0;
		while (n != 1) {	//cicla fino a controllo != 1

			scanf("%d", &s);	//acquisizone input

			if (s == 1 || s == 0)	//se input == 1 o 0 esci dal ciclo
				n++;
			else
				printf("ERRORE, REINSERIRE SCELTA.\n"); //altrimenti stampa errore
		}

		if (s == 1) {

			ptr = fopen("tmp.txt", "wb");	//apertura file in modalit� scrittura binaria distruttiva
			fclose(ptr);	//chiusura del file

			for (i = 0; i < k; i++) {	//ciclo for da indirizzo 0 a indirizzo k, con apertura del file preference
				//in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("preference.dat", "rb")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(preferenza), SEEK_SET);
					fread(&preferenzatemp, sizeof(preferenza), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(preferenza), SEEK_SET);
					fwrite(&preferenzatemp, sizeof(preferenza), 1, ptr);
				}
				fclose(ptr);
			}

			n = k;	//assegna valore indirizzo k a n e incrementa n di 1
			n++;

			for (i = n; i < j; i++) {	//ciclo for da indirizzo n a indirizzo j (indirizzi totali), con apertura del file preference
				//in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file tmp in modalit� scrittura binaria, scrittura della struct in memoria in posizione k
				//e chiusura del file

				if ((ptr = fopen("preference.dat", "rb")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(preferenza), SEEK_SET);
					fread(&preferenzatemp, sizeof(preferenza), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("tmp.txt", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, k * sizeof(preferenza), SEEK_SET);
					fwrite(&preferenzatemp, sizeof(preferenza), 1, ptr);
				}
				k++;
				fclose(ptr);
			}

			ptr = fopen("preference.dat", "wb");	//apertura file in modalit� scrittura binaria distruttiva
			fclose(ptr);	//chiusura del file

			for (i = 0; i < k; i++) {	//ciclo for da indirizzo 0 a indirizzo k (indirizzi totali),
				//con apertura del file tmp in modalit� lettura, caricamento della struct in memoria, chiusura del file
				//apertura file preference in modalit� scrittura binaria, scrittura della struct in memoria
				//e chiusura del file

				if ((ptr = fopen("tmp.txt", "rb")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(preferenza), SEEK_SET);
					fread(&preferenzatemp, sizeof(preferenza), 1, ptr);
				}
				fclose(ptr);

				if ((ptr = fopen("preference.dat", "rb+")) == NULL) {
					printf("ERRORE FILE.\n");
				} else {
					fseek(ptr, i * sizeof(preferenza), SEEK_SET);
					fwrite(&preferenzatemp, sizeof(preferenza), 1, ptr);
				}
				fclose(ptr);
			}
			printf("\nPreferenza Eliminata con Successo!\n");
			system("pause");
		} else {	//se s != 1 stampa messaggio
			printf("\n!!!PREFERENZA NON ELIMINATA!!!\n");
			system("pause");
		}
	} else {	//se uguale != 1 stampa messaggio
		printf("\n!!!ARTISTA NON TROVATO!!!\n");
		system("pause");
	}


	return 0;

}

int calcolo_artisti(FILE *ptr) {	//funzione per calcolo numero artisti per creazione matrice

	x = 0;

	if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&artista, sizeof(artista), 1, ptr); 	//lettura struct
		}

		x = artista.id_artista;	//assegna a x ultimo id

	}
	fclose(ptr);	//chiusura del file

	return 0;

}

int calcolo_utenti(FILE *ptr) {	//funzione per calcolo numero utenti per creazione matrice

	y = 0;

	if ((ptr = fopen("user.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		rewind(ptr);									//posiziona puntatore all'inizio file
		while (!feof(ptr)) {							//cicla puntatore fino a fine file
			fread(&utente, sizeof(utente), 1, ptr);		//carica in memoria struct utenti e aumenta contatore indirizzi
			y++;
		}

		y--;	//sottrae di 1 contatore indirizzi per indicare correttamente ultimo indirizzo nel file
	}
	fclose(ptr);	//chiusura del file

	return 0;

}

int reccomender_system(FILE *ptr) {	//funzione per reccomender system

	int mat[x][y];			//dichiarazione matrice
	int intersezione = 0;	//dichiarazione e inizializzazione variabile intersezione
	int unione = 0;			//dichiarazione e inizializzazione variabile unione
	float jaccard[y];			//dichiarazione array con coefficienti
	float coefficiente = 0;	//dichiarazione e inizializzazione variabile coefficiente
	int u_simile = 0;		//dichiarazione e inizializzazione variabile utente simile
	int cont_artisti = 0;	//dichiarazione e inizializzazione variabile contatore artisti

	for (i = 0; i < x; i++) {	//ciclo for azzeramento matrice
		for (j = 0; j < y; j++) {
			mat[i][j] = 0;
		}
	}

	for (i = 0; i < y; i++) {	//ciclo for azzeramento array
		jaccard[i] = 0;
	}

	for (i = 0; i < x; i++) {	//ciclo for da indirizzo 0 a x con caricamento in memoria della struct artisti

		if ((ptr = fopen("artist.dat", "rb")) == NULL) {
			printf("ERRORE FILE.\n");
		} else {

			fseek(ptr, i * sizeof(artista), SEEK_SET);
			fread(&artista, sizeof(artista), 1, ptr);

		}
		fclose(ptr);

		for (j = 0; j < y; j++) {	//ciclo for da indirizzo 0 a y con caricamento in memoria della struct utenti

			if ((ptr = fopen("user.dat", "rb")) == NULL) {
				printf("ERRORE FILE.\n");
			} else {

				fseek(ptr, j * sizeof(utente), SEEK_SET);
				fread(&utente, sizeof(utente), 1, ptr);

			}
			fclose(ptr);

			if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
				printf("ERRORE FILE.\n");

			} else {

				k = 0;
				rewind(ptr);													//posiziona puntatore all'inizio file
				while (!feof(ptr)) {											//cicla puntatore fino a fine file
					fseek(ptr, k * sizeof(preferenza), SEEK_SET);				//posiziona puntatore a indirizzo k
					fread(&preferenzatemp, sizeof(preferenza), 1, ptr);			//carica struct in memoria
					if (!strcmp(preferenzatemp.username, utente.username)		//compara struct preference con struct
							&& !strcmp(preferenzatemp.artista, artista.nome)	//artista e utente e verifica la presenza
							&& !strcmp(preferenzatemp.preferenza, like)) {		//del campo like contenente like
						mat[i][j]++;											//se condizioni soddisfatte incrementa valore matrice in posizione i j
						break;
					}
					k++;	//incremento contatore indirizzo
				}
			}
			fclose(ptr);
		}


	}

	for (j = 0; j < y; j++) {	//ciclo for che scorre tutti gli utenti

		intersezione = 0;
		unione = 0;
		coefficiente = 0;

		if (j != id_utente) {	//se indice != da utente loggato prosegui

			for (n = 0; n < x; n++) {

				if (mat[n][j] == 1 && mat[n][id_utente] == 1)	//se posizione ciclo della matrice == 1 e posizione
					//matrice con stesso artista e utente loggato == 1
					intersezione++;								//incrementa valore intersezione

				if (mat[n][j] == 1 || mat[n][id_utente] == 1)	//se posizione ciclo della matrice == 1 o posizione
					//matrice con stesso artista e utente loggato == 1
					unione++;									//incrementa valore unione

			}

			float coefficiente = (float)intersezione / unione;	//calcolo coefficiente di jaccard

			jaccard[j] = coefficiente;	//assegna a posizione j dell'array valore coefficiente
		}

	}

	for (i=0; i<y; i++)

		coefficiente = 0;	//azzera coefficiente

	for (j = 0; j < y; j++) {				//ciclo for che analizza il vettore e trova il coefficiente pi� alto
		if (jaccard[j] > coefficiente) {	//il coefficiente pi� alto corrisponde all'utente j, che viene salvato
			coefficiente = jaccard[j];
			u_simile = j;
		}
	}

	cont_artisti = 0;

	n = 0;

	for (i = 0; i < x; i++) {	//ciclo for che analizza tutti gli artisti

		cont_artisti = 0;

		if (mat[i][u_simile] == 1) {	//se posizione mat[i][utente simile] contenente like entra nel ciclo

			for (j = 0; j < y; j++) {	//ciclo for che analizza tutti gli utenti

				if (mat[i][j] == 1)	//se posizione contiene like aumenta il contatore artista
					cont_artisti++;
			}
		}

		//il contatore corrisponde all'artista i fino a quando quest'ultimo viene incrementato

		if (cont_artisti > n && mat[i][id_utente] == 0) {	//se contatore artista > contatore n e non � presente like
			//all'artista da parte di utente loggato assegna cont_artisti
			//a n e salva id artista in reccomender
			n = cont_artisti;
			reccomender = i;

		}
	}

	if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");
	} else {

		fseek(ptr, reccomender * sizeof(artista), SEEK_SET);	//posiziona puntatore a indirizzo reccomender e carica
		fread(&artista, sizeof(artista), 1, ptr);				//in memoria struct artista

	}
	fclose(ptr);

	k = 0;

	if (isalpha(artista.nome[k]))
	{
		artista.nome[k] = toupper(artista.nome[k]);
	}

	while (artista.nome[k]) {
		if (artista.nome[k] == '_')
		{

			artista.nome[k] = ' ';
			k++;
			if (isalpha(artista.nome[k]))
				artista.nome[k] = toupper(artista.nome[k]);

			k--;
		}
		k++;
	}

	printf(" Utenti simili ascoltano l'artista %s, ascoltalo anche tu!\n\n",artista.nome);	//stampa artista raccomandato

	return 0;

}


int ordinamento_listen(FILE *ptr) { //funzione ordinamento ascolti

	int artisti[x];			//dichiarazione vettore contenente numero ascolti artisti
	int art_ordinati[x];	//dichiarazione vettore contenente numero ascolti artisti ordinato
	int temp = 0;			//dichiarazione e inizializzazione variabile temporanea

	for (i = 0; i < x; i++) {	//ciclo for azzeramento matrice
		artisti[i] = 0;
	}

	for (i = 0; i < x; i++) {	//ciclo for azzeramento matrice
		art_ordinati[i] = 0;
	}

	if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		n = 0;
		rewind(ptr);											//posiziona puntatore all'inizio file
		while (!feof(ptr)) {									//cicla puntatore fino a fine file
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);	//carica in memoria struct preferenza e aumenta contatore
			n++;
		}

		n--;	//decrementa contatore di 1 per correggere ultimo indirizzo

	}
	fclose(ptr);


	for (i = 0; i < x; i++) {	//ciclo for da indirizzo 0 a x con caricamento in memoria della struct artisti

		if ((ptr = fopen("artist.dat", "rb")) == NULL) {
			printf("ERRORE FILE.\n");
		} else {

			fseek(ptr, i * sizeof(artista), SEEK_SET);
			fread(&artista, sizeof(artista), 1, ptr);

		}
		fclose(ptr);


		if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
			printf("ERRORE FILE.\n");

		} else {

			rewind(ptr);													//posiziona puntatore all'inizio file
			for (k=0; k<n; k++) {											//ciclo for da 0 a ultimo indirizzo
				fseek(ptr, k * sizeof(preferenza), SEEK_SET);				//posiziona puntatore a indirizzo k
				fread(&preferenzatemp, sizeof(preferenza), 1, ptr);			//carica struct in memoria
				if (!strcmp(preferenzatemp.artista, artista.nome)			//artista e utente e verifica la presenza
						&& !strcmp(preferenzatemp.preferenza, listen)) {	//del campo listen contenente listen
					artisti[i]++;											//se condizioni soddisfatte incrementa valore vettore
				}
			}
		}
		fclose(ptr);
	}

	for (i = 0; i < x; i++) {			//duplicazione vettore
		art_ordinati[i] = artisti[i];
	}

	for(i = 0; i<x-1; i++) {				//ordinamento bubble sort vettore
		for(k = 0; k<x-1-i; k++) {
			if(art_ordinati[k] < art_ordinati[k+1]) {
				temp = art_ordinati[k];
				art_ordinati[k] = art_ordinati[k+1];
				art_ordinati[k+1] = temp;
			}
		}
	}

	system("cls");
	printf("\n	SPOTIFY - Classifica per Listen\n\n");
	printf(" %-8s %s\n\n", "POSIZIONE", "ARTISTA");

	if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		n = 1;	//contatore posizioni

		for (i=0; i<x; i++) {	//ciclo for da 0 a x (numero artisti)

			fseek(ptr, i * sizeof(artista), SEEK_SET);	//posiziona puntatore a indirizzo i carica in memoria struct
			fread(&artista, sizeof(artista), 1, ptr);

			z = 0;

			if (isalpha(artista.nome[z]))
			{
				artista.nome[z] = toupper(artista.nome[z]);
			}

			while (artista.nome[z]) {
				if (artista.nome[z] == '_')
				{

					artista.nome[z] = ' ';
					z++;
					if (isalpha(artista.nome[z]))
						artista.nome[z] = toupper(artista.nome[z]);

					z--;
				}
				z++;
			}

			printf(" %-10d",n);

			for(k=0;k<x;k++) {	//ciclo for che analizza tutti gli indici artisti

				if(art_ordinati[i] == artisti[k]) {			//se valore in vettore art_ordinati == valore in artisti
					printf("%s\n\n",artista.nome);			//stampa nome artista
					break;
				}
			}
			n++;	//incremento contatore
		}
	}
	fclose(ptr);	//chiusura del file

	system("pause");

	return 0;

}

int ordinamento_like(FILE *ptr) { //funzione di ordinamento mi piace

	int artisti[x];			//dichiarazione vettore contenente numero ascolti artisti
	int art_ordinati[x];	//dichiarazione vettore contenente numero ascolti artisti ordinato
	int temp = 0;			//dichiarazione e inizializzazione variabile temporanea

	for (i = 0; i < x; i++) {	//ciclo for azzeramento matrice
		artisti[i] = 0;
	}

	for (i = 0; i < x; i++) {	//ciclo for azzeramento matrice
		art_ordinati[i] = 0;
	}

	if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		n = 0;
		rewind(ptr);											//posiziona puntatore all'inizio file
		while (!feof(ptr)) {									//cicla puntatore fino a fine file
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);	//carica in memoria struct preferenza e aumenta contatore
			n++;
		}

		n--;	//decrementa contatore di 1 per correggere ultimo indirizzo

	}
	fclose(ptr);


	for (i = 0; i < x; i++) {	//ciclo for da indirizzo 0 a x con caricamento in memoria della struct artisti

		if ((ptr = fopen("artist.dat", "rb")) == NULL) {
			printf("ERRORE FILE.\n");
		} else {

			fseek(ptr, i * sizeof(artista), SEEK_SET);
			fread(&artista, sizeof(artista), 1, ptr);

		}
		fclose(ptr);


		if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
			printf("ERRORE FILE.\n");

		} else {

			rewind(ptr);													//posiziona puntatore all'inizio file
			for (k=0; k<n; k++) {											//ciclo for da 0 a ultimo indirizzo
				fseek(ptr, k * sizeof(preferenza), SEEK_SET);				//posiziona puntatore a indirizzo k
				fread(&preferenzatemp, sizeof(preferenza), 1, ptr);			//carica struct in memoria
				if (!strcmp(preferenzatemp.artista, artista.nome)			//artista e utente e verifica la presenza
						&& !strcmp(preferenzatemp.preferenza, like)) {		//del campo listen contenente like
					artisti[i]++;
				}
			}
		}
		fclose(ptr);
	}

	for (i = 0; i < x; i++) {			//duplicazione vettore
		art_ordinati[i] = artisti[i];
	}

	for(i = 0; i<x-1; i++) {				//ordinamento bubble sort vettore
		for(k = 0; k<x-1-i; k++) {
			if(art_ordinati[k] < art_ordinati[k+1]) {
				temp = art_ordinati[k];
				art_ordinati[k] = art_ordinati[k+1];
				art_ordinati[k+1] = temp;
			}
		}
	}

	system("cls");
	printf("\n	SPOTIFY - Classifica per Like\n\n");
	printf(" %-8s %s\n\n", "POSIZIONE", "ARTISTA");

	if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		n = 1;	//contatore posizioni

		for (i=0; i<x; i++) {	//ciclo for da 0 a x (numero artisti)

			fseek(ptr, i * sizeof(artista), SEEK_SET);	//posiziona puntatore a indirizzo i carica in memoria struct
			fread(&artista, sizeof(artista), 1, ptr);

			z = 0;

			if (isalpha(artista.nome[z]))
			{
				artista.nome[z] = toupper(artista.nome[z]);
			}

			while (artista.nome[z]) {
				if (artista.nome[z] == '_')
				{

					artista.nome[z] = ' ';
					z++;
					if (isalpha(artista.nome[z]))
						artista.nome[z] = toupper(artista.nome[z]);

					z--;
				}
				z++;
			}

			printf(" %-10d",n);

			for(k=0;k<x;k++) {	//ciclo for che analizza tutti gli indici artisti

				if(art_ordinati[i] == artisti[k]) {			//se valore in vettore art_ordinati == valore in artisti
					printf("%s\n\n",artista.nome);			//stampa nome artista
					break;
				}
			}
			n++;	//incremento contatore
		}
	}
	fclose(ptr);	//chiusura del file

	return 0;

}

int ordinamento_unlike(FILE *ptr) { //funzione di ordinamento non mi piace

	int artisti[x];			//dichiarazione vettore contenente numero ascolti artisti
	int art_ordinati[x];	//dichiarazione vettore contenente numero ascolti artisti ordinato
	int temp = 0;			//dichiarazione e inizializzazione variabile temporanea

	for (i = 0; i < x; i++) {	//ciclo for azzeramento matrice
		artisti[i] = 0;
	}

	for (i = 0; i < x; i++) {	//ciclo for azzeramento matrice
		art_ordinati[i] = 0;
	}

	if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		n = 0;
		rewind(ptr);											//posiziona puntatore all'inizio file
		while (!feof(ptr)) {									//cicla puntatore fino a fine file
			fread(&preferenzatemp, sizeof(preferenza), 1, ptr);	//carica in memoria struct preferenza e aumenta contatore
			n++;
		}

		n--;	//decrementa contatore di 1 per correggere ultimo indirizzo

	}
	fclose(ptr);


	for (i = 0; i < x; i++) {	//ciclo for da indirizzo 0 a x con caricamento in memoria della struct artisti

		if ((ptr = fopen("artist.dat", "rb")) == NULL) {
			printf("ERRORE FILE.\n");
		} else {

			fseek(ptr, i * sizeof(artista), SEEK_SET);
			fread(&artista, sizeof(artista), 1, ptr);

		}
		fclose(ptr);


		if ((ptr = fopen("preference.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
			printf("ERRORE FILE.\n");

		} else {

			rewind(ptr);													//posiziona puntatore all'inizio file
			for (k=0; k<n; k++) {											//ciclo for da 0 a ultimo indirizzo
				fseek(ptr, k * sizeof(preferenza), SEEK_SET);				//posiziona puntatore a indirizzo k
				fread(&preferenzatemp, sizeof(preferenza), 1, ptr);			//carica struct in memoria
				if (!strcmp(preferenzatemp.artista, artista.nome)			//artista e utente e verifica la presenza
						&& !strcmp(preferenzatemp.preferenza, unlike)) {	//del campo listen contenente unlike
					artisti[i]++;											//se condizioni soddisfatte incrementa valore vettore
				}
			}
		}
		fclose(ptr);
	}

	for (i = 0; i < x; i++) {			//duplicazione vettore
		art_ordinati[i] = artisti[i];
	}

	for(i = 0; i<x-1; i++) {				//ordinamento bubble sort vettore
		for(k = 0; k<x-1-i; k++) {
			if(art_ordinati[k] < art_ordinati[k+1]) {
				temp = art_ordinati[k];
				art_ordinati[k] = art_ordinati[k+1];
				art_ordinati[k+1] = temp;
			}
		}
	}

	system("cls");
	printf("\n	SPOTIFY - Classifica per Unlike\n\n");
	printf(" %-8s %s\n\n", "POSIZIONE", "ARTISTA");

	if ((ptr = fopen("artist.dat", "rb")) == NULL) {	//apertura file in modalit� lettura binaria
		printf("ERRORE FILE.\n");

	} else {

		n = 1;	//contatore posizioni

		for (i=0; i<x; i++) {	//ciclo for da 0 a x (numero artisti)

			fseek(ptr, i * sizeof(artista), SEEK_SET);	//posiziona puntatore a indirizzo i carica in memoria struct
			fread(&artista, sizeof(artista), 1, ptr);

			z = 0;

			if (isalpha(artista.nome[z]))
			{
				artista.nome[z] = toupper(artista.nome[z]);
			}

			while (artista.nome[z]) {
				if (artista.nome[z] == '_')
				{

					artista.nome[z] = ' ';
					z++;
					if (isalpha(artista.nome[z]))
						artista.nome[z] = toupper(artista.nome[z]);

					z--;
				}
				z++;
			}

			printf(" %-10d",n);

			for(k=0;k<x;k++) {	//ciclo for che analizza tutti gli indici artisti

				if(art_ordinati[i] == artisti[k]) {			//se valore in vettore art_ordinati == valore in artisti
					printf("%s\n\n",artista.nome);			//stampa nome artista
					break;
				}
			}
			n++;	//incremento contatore
		}
	}
	fclose(ptr);	//chiusura del file

	return 0;

}


