/*
 * prototipi.h
 *
 *  Created on: 10 set 2017
 *      Author: fabio
 */

#ifndef PROTOTIPI_H_
#define PROTOTIPI_H_

#include "lib.h"
#include "struct.h"
#include "globali.h"


//prototipi di funzione
int creazione_artisti(FILE *ptr);
int aggiunta_artisti(FILE *ptr);
int visualizzazione_artisti(FILE *ptr);
char conversione(char le);
int modifica_artisti(FILE *ptr);
int eliminazione_artisti(FILE *ptr);
date inserimento_data();
int creazione_utenti(FILE *ptr);
int aggiunta_utenti(FILE *ptr);
int visualizzazione_utenti(FILE *ptr);
int modifica_utenti(FILE *ptr);
int eliminazione_utenti(FILE *ptr);
int login_admin(FILE *ptr);
int login(FILE *ptr);
int registrazione(FILE *ptr);
int ricerca(FILE *ptr);
int risultati(void);
int Generatore(int ins);
int controlloPass();
int sicurezzaPass(int contpass);
int ascolto(FILE *ptr);
int profilo(FILE *ptr);
int mod_pref_utente(FILE *ptr);
int del_pref_utente(FILE *ptr);
int calcolo_artisti(FILE *ptr);
int calcolo_utenti(FILE *ptr);
int reccomender_system(FILE *ptr);
int ordinamento_listen(FILE *ptr);
int ordinamento_like(FILE *ptr);
int ordinamento_unlike(FILE *ptr);

#endif /* PROTOTIPI_H_ */
