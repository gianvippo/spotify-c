/*
 * struct.h
 *
 *  Created on: 10 set 2017
 *      Author: fabio
 */

#ifndef STRUCT_H_
#define STRUCT_H_


typedef struct {	//struct data
	int giorno;		//campo giorno
	int mese;		//campo mese
	int anno;		//campo anno
} date;

typedef struct {			//struct artista
	int id_artista;			//campo id artista
	char nome[26];			//campo nome artista
	char genere[16];		//campo genere artista
	char produttore[31];	//campo produttore artista
	char nazionalita[16];	//campo nazionalitą artista
	int anno_attivita;		//campo anno attivitą artista
} artist;

typedef struct {		//struct utente
	char username[26];	//campo username utente
	char password[9];	//campo password utente
	char nome[16];		//campo nome utente
	char cognome[16];	//campo cognome utente
	date data_nascita;	//campo data di nascita utente
	date data_iscr;		//campo data di iscrizione utente
} user;

typedef struct {		//struct preferenza utente
	char username[26];	//campo username utente
	char artista[26];	//campo nome dell'artista
	char preferenza[7]; //campo preferenza
} preference;


#endif /* STRUCT_H_ */
